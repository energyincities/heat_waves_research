from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy

def lighting_parameters():
    """
    A parameter set that modifies overall lighting characteristics for the building
    """
    
    group_name="Lighting"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    # This is the installed lighting power density, in W/m2 of general office area.
    # 5 W/m2 is easily achievable with new LED technology. Even lower for a well-designed space. For consistency, however, we will use 10.6 W/m2 based on open office from table 4.2.1.6 of NECB 2015.
    parameters.append(Parameter(name='Overhead Lighting Power Density (W/m2)',
                               selector=FieldSelector(class_name='Lights',
                                                      object_name='Overhead Lighting',field_name='Watts per Zone Floor Area'),
                               value_descriptor=RangeParameter(min_val=0,max_val=20))) 
    defaults.update({parameters[-1].value_descriptors[0].name:10.6})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),        
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,        
        "Displayed Order": None,         
        "Visibility": "Shown",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)

    #This is task lighting. It has been set up to follow the exact same schedule as the occupancy. It could also be a fudge-factor for plug loads dependent on occupancy. 
    # A personal LED task light is probably around 5W, so that would be a reasonable default, but quite often it is not installed, so we are using 0W.
    parameters.append(Parameter(name='Task Lighting Power Density (W/occ)',
                               selector=FieldSelector(class_name='Lights',
                                                      object_name='Task Lighting',field_name='Watts per Person'),
                               value_descriptor=RangeParameter(min_val=0,max_val=200))) 
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,         
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is used to modify the fraction of overhead lights that are controlled by daylight sensors. Currently task lighting occurs irrespective of daylighting.
    # This could be modified to include the tasklighting aswell. 
    # Difficult to decide on a default value here. A value of 1 would be considered all lghts in perimeter zones are controlled by the central sensors. Maybe a default of 0.5?
    parameters.append(Parameter(name='Daylighting Fraction',
                               selector=FieldSelector(class_name='Daylighting:Controls',
                                                      object_name='*',field_name='Fraction of Zone Controlled by Reference Point 1'),
                               value_descriptor=RangeParameter(min_val=0,max_val=1))) 
    defaults.update({parameters[-1].value_descriptors[0].name:.5})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,        
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This defines the setpoint for work-level lux for daylighting to maintain. 
    # A default value of 500 would be reasonable
    parameters.append(Parameter(name='Daylighting Threshold (Lux)',
                               selector=FieldSelector(class_name='Daylighting:Controls',
                                                      object_name='*',field_name='Illuminance Setpoint at Reference Point 1'),
                               value_descriptor=RangeParameter(min_val=300,max_val=1000))) 
    defaults.update({parameters[-1].value_descriptors[0].name:500})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #External Lighting- controlled via astronomical clock

    parameters.append(Parameter(name='External Lighting (W)',
                               selector=FieldSelector(class_name='Exterior:Lights',
                                                      object_name='Exterior Lights',field_name='Design Level'),
                               value_descriptor=RangeParameter(min_val=0,max_val=100000))) 
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Long Name": None,         
        "Description": None,         
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
        ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Lighting parameters start at 400
    
    def Display_Order(parameter_metadata_list):
        x = 400
        for metadata_dict in parameter_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(parameter_metadata_list)

    return parameters, defaults, parameter_metadata_list
