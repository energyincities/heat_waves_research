from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy

def comfort_parameters():
    
    """
    A parameter set that modifies the occupancy and indoor air quality
    """
    
    group_name="Occupancy and Indoor Conditions"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This is floor area per person, the inverse of density. 
    # Default value of 25 m2/person I believe matches NECB office building-type
    parameters.append(Parameter(name='Occupancy (m2/occ)',
                               selector=FieldSelector(class_name='People',
                                                      object_name='*',field_name='Zone Floor Area per Person'),
                               value_descriptor=RangeParameter(min_val=1,max_val=100)))
    defaults.update({parameters[-1].value_descriptors[0].name:25})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("The floor area per person available at peak capacity, the inverse of density. [Additional Info](https://buildingcodetrainer.com/building-occupancy-classification-occupancy-types-explained/)"),
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,         
        "Displayed Order": None,         
        "Visibility": "Shown",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is the temperature setpoint during operating hours. Needs to be revised in the future w/ cooling for a middle / deadband type control.
    # Default should be 21. 
    parameters.append(Parameter(name='Heating Setpoint (C)',
                               selector=FieldSelector(class_name='Schedule:Day:Interval',
                                                      object_name='NECB-A-Thermostat Setpoint-Heating Default',field_name='Value Until Time 2'),
                               value_descriptor=RangeParameter(min_val=18,max_val=22)))
    defaults.update({parameters[-1].value_descriptors[0].name:21})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("The temperature the builidng is heated to during operating hours. [Additional Info](https://knowledge.autodesk.com/support/revit-products/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/Revit-Model/files/GUID-CB19C544-61FB-4E1B-BC97-882822B157A3-htm.html#:~:text=Heating%20Set%20Point%3A%20Temperature%20at,is%20specified%20for%20each%20zone.)"),     
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,   
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is the temperature setpoint during operating hours. Needs to be revised in the future w/ heating for a middle / deadband type control.
    # Default should be 24. 
    parameters.append(Parameter(name='Cooling Setpoint (C)',
                               selector=FieldSelector(class_name='Schedule:Day:Interval',
                                                      object_name='NECB-A-Thermostat Setpoint-Cooling Default',field_name='Value Until Time 2'),
                               value_descriptor=RangeParameter(min_val=22.1,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:24})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("The temperature the builidng is cooled to during operating hours. [Additional Info](https://knowledge.autodesk.com/support/revit-products/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/Revit-Model/files/GUID-CB19C544-61FB-4E1B-BC97-882822B157A3-htm.html#:~:text=Heating%20Set%20Point%3A%20Temperature%20at,is%20specified%20for%20each%20zone.)"),   
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,   
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is the low-end of the relative humidity setpoint within the office zones. The Air system will be controlled to maintain the between the low and high setpoints.
    # Default, I think, should be an RH of 30. 
    parameters.append(Parameter(name='RH-Low Setpoint (%RH)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='RH Low',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=0,max_val=40)))
    defaults.update({parameters[-1].value_descriptors[0].name:30})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Relative Humidity - Low Setpoint",         
        "Description": ("The minimum RH that will be maintained during operating hours. Also know as the Humidification Setpoint. [Additional Info](https://knowledge.autodesk.com/support/revit-products/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/Revit-Model/files/GUID-CB19C544-61FB-4E1B-BC97-882822B157A3-htm.html#:~:text=Heating%20Set%20Point%3A%20Temperature%20at,is%20specified%20for%20each%20zone.)"),       
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,   
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is the high-end of the relative humidity setpoint within the office zones. The Air system will be controlled to maintain the between the low and high setpoints.
    # Default, I think, should be an RH of 60. 
    parameters.append(Parameter(name='RH-High Setpoint (%RH)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='RH High',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=41,max_val=100)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Relative Humidity - High Setpoint",         
        "Description": ("The maximum RH that will be maintained during operating hours. Also know as the Dehumidification Setpoint. [Additional Info](https://knowledge.autodesk.com/support/revit-products/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/Revit-Model/files/GUID-CB19C544-61FB-4E1B-BC97-882822B157A3-htm.html#:~:text=Heating%20Set%20Point%3A%20Temperature%20at,is%20specified%20for%20each%20zone.)"),   
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,       
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # This is m3/s/occupant in the office areas of the building. 
    # Table 6-1 of ASHRAE 62.1-2010 identifies office space to require 2.5 L/s/occ, or 0.0025 m3/s/occ.
    
    def occ_vent_convert(building,value):
        vent_per_occ=value/1000
        FieldSelector(class_name='DesignSpecification:OutdoorAir',object_name='Office Ventilation',field_name='Outdoor Air Flow per Person').set(building,vent_per_occ)
        return
    
    parameters.append(Parameter(name='Ventilation Per Occupancy (L/s-occ)',
                               selector=GenericSelector(set=occ_vent_convert),
                               value_descriptor=RangeParameter(min_val=0,max_val=25)))
    defaults.update({parameters[-1].value_descriptors[0].name:2.5})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,    
        "Description": ("The amount of air per second per person required to ventilate a space. [Additional Info](https://bigladdersoftware.com/epx/docs/8-9/input-output-reference/group-design-objects.html#field-outdoor-air-flow-per-person)"),
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,              
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #m3/s/m2 of floor area in the office areas.
    # Default of 0.0003 m3/s/m2
    def area_vent_convert(building,value):
        vent_per_area=value/1000
        FieldSelector(class_name='DesignSpecification:OutdoorAir',object_name='Office Ventilation',field_name='Outdoor Air Flow per Zone Floor Area').set(building,vent_per_area)
        return
    
    parameters.append(Parameter(name='Ventilation Per Floor Area (L/s-m2)',
                               selector=GenericSelector(set=area_vent_convert),
                               value_descriptor=RangeParameter(min_val=0,max_val=3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.3})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("The amount of air per second per floor area required to ventilate a space. [Additional Info](https://bigladdersoftware.com/epx/docs/8-9/input-output-reference/group-design-objects.html#field-outdoor-air-flow-per-zone-floor-area)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,        
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This will add all of the baseline ventilation requirements and multiply it by a value. This will be used to represent ventilation efficiency in the system together with distribution effectiveness within the zone.
    # A default of 0.80 represents a 100% efficiency and distribution effectiveness of 0.8 in the zone, which would be reasonable for a DOAS supplying neutral air. 
    parameters.append(Parameter(name='Ventilation Distribution Effectiveness',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='Typical ZAD Schedule',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=0.6,max_val=1.4)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.8})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("The value between 1 and 0 desribing the effectiveness(or efficiency) or the ventilation distribution system. [Additional Info](https://bigladdersoftware.com/epx/docs/8-9/input-output-reference/group-design-objects.html#field-name-1-010)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Comfort parameters start at 200
    
    def Display_Order(parameter_metadata_list):
        x = 200
        for metadata_dict in parameter_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(parameter_metadata_list)
    
    return parameters, defaults, parameter_metadata_list
