from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy

def envelope_parameters():
    
    """
    A parameter set that modifies the envelope parameters of the archetype buildings
    """
    
    group_name="Envelope"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This is 'meters' of wall insulation. The wall insulation has a conductivity of 0.049 W/mK, or about R3 per inch. 0.01 translates to R-3.55, 0.5 translates to R-60. 
    # Default, I think, would be R-14, for lack of better choice? That's about 0.09 meters, with some error there for now. 
    
    def wall_r_value(building,value):
        ins_rvalue=value-2.39
        ins_rsivalue=ins_rvalue/5.678
        ins_thickness=ins_rsivalue*0.049
        FieldSelector(class_name='Material',object_name='Typical Wall Insulation',field_name='Thickness').set(building,ins_thickness)
        return
    
    parameters.append(Parameter(name='Wall Insulation (R-Value)',
                               selector=GenericSelector(set=wall_r_value),
                               value_descriptor=RangeParameter(min_val=3.5,max_val=60)))     
    defaults.update({parameters[-1].value_descriptors[0].name:14})
    
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),        
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,         
        "Displayed Order": None,         
        "Visibility": "Shown",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is 'meters' of roof insulation. Conductivity of 0.049 W/mK. 0.01 translates to R-1.94, 0.8 translates to R-93. This needs to be preprocessed here for the final submission. 
    # Default, I would guess a reasonable number is R20? That's about 0.15m, give or take. 
    
    def roof_r_value(building,value):
        ins_rvalue=value-0.79
        ins_rsivalue=ins_rvalue/5.678
        ins_thickness=ins_rsivalue*0.049
        FieldSelector(class_name='Material',object_name='Typical Roof Insulation',field_name='Thickness').set(building,ins_thickness)
        return
    
    parameters.append(Parameter(name='Roof Insulation (R-Value)',
                               selector=GenericSelector(set=roof_r_value),
                               value_descriptor=RangeParameter(min_val=5,max_val=90)))     
    defaults.update({parameters[-1].value_descriptors[0].name:20})
    
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,   
        "Displayed Order": None,         
        "Visibility": "Shown",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)

    #Glazing USI- this is directly, W/m2K of the glazing system as a whole. 
    # I would set the default to 2.2 W/m2K as the baseline number for NECB. (This may have changed in the latest edition, but for now its safe). 
    parameters.append(Parameter(name='Glazing U-value (W/m2K)',
                               selector=FieldSelector(class_name='WindowMaterial:SimpleGlazingSystem',
                                                      object_name='SimpleGlazing',field_name='U-Factor'),
                               value_descriptor=RangeParameter(min_val=0.5,max_val=5.5)))
    defaults.update({parameters[-1].value_descriptors[0].name:2.2})
    
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Shown",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Solar heat gain coefficient of the glazing system as a whole. 
    # Default of 0.40 (This was the limit where the reference matches the proposed in most standards)
    parameters.append(Parameter(name='Glazing SHGC',
                               selector=FieldSelector(class_name='WindowMaterial:SimpleGlazingSystem',
                                                      object_name='SimpleGlazing',field_name='Solar Heat Gain Coefficient'),
                               value_descriptor=RangeParameter(min_val=0.1,max_val=0.8)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.4})
    
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Glazing Solar Heat Gain Coefficient",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # A default of 0.7 is reasonable for most window selections. 
    parameters.append(Parameter(name='Glazing Visible Transmittance',
                               selector=FieldSelector(class_name='WindowMaterial:SimpleGlazingSystem',
                                                      object_name='SimpleGlazing',field_name='Visible Transmittance'),
                               value_descriptor=RangeParameter(min_val=0.3,max_val=0.9)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.7})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is 'meters' of concrete in the wall. The current setup as concrete on the inside of the insulation. The concrete has a specific heat of 1,000 J/kgK and a density of 1400 kg/m3. Future parameter could be 'exterior' concrete mass. Thermal mass or meters will be the final unit - to be determined. 
    # I would say the default should be a light-weight wall, so 0.01
    parameters.append(Parameter(name='Wall Thermal Mass (m)',
                               selector=FieldSelector(class_name='Material',
                                                      object_name='Wall Mass',field_name='Thickness'),
                               value_descriptor=RangeParameter(min_val=0.01,max_val=0.1))) 
    defaults.update({parameters[-1].value_descriptors[0].name:0.01})
    
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Same as wall concrete.
    #Values greater than 0.08 are breaking the model's CTF calculation.
    parameters.append(Parameter(name='Floor Thermal Mass (m)',
                               selector=FieldSelector(class_name='Material',
                                                      object_name='Floor Mass',field_name='Thickness'),
                               value_descriptor=RangeParameter(min_val=0.01,max_val=0.08))) 
    defaults.update({parameters[-1].value_descriptors[0].name:0.015})
    
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #this is effective F-factor of the slab-on-grade. Units are W/mK. 
    # For default value, I would say 2 ft of vertical R10? Which is 0.54(IP) or 0.93 in W/mK
    parameters.append(Parameter(name='Slab Insulation (W/mK)',
                               selector=FieldSelector(class_name='Construction:FfactorGroundFloor',
                                                      object_name='*',field_name='F-Factor'),
                               value_descriptor=RangeParameter(min_val=0.0001,max_val=2)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.93})
    
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Baseline infiltration rate. Units are m3/s/m2 of exterior surface area, include walls, windows, and roofs. 
    # Default should be NECB value at 0.00025m3/s/m2
    #These parameters modify how the infiltration is calculated.
    #Range values from: PNNL study on E+ infiltration https://www.pnnl.gov/main/publications/external/technical_reports/PNNL-18898.pdf
    #A future parameter could be whether or not to schedule infiltration differently of the mechanical system is operating, under the assumption that buildings are generally pressurized (~5pa) and all outdoor air influx into the building would be through the mechanical system if its operating. 
    
    #Infiltration rate is set according to the following equation
    #Infiltration = I_design * F_schedule * (Constant + Temperature_factor*(tempdiff) + Wind_factor*(windspeed))#Modify units to L/s/m2 at 5 Pa
    
    
    def litre_to_m3(building,value):
        inf_val=value/1000
        FieldSelector(class_name='ZoneInfiltration:DesignFlowRate',object_name='*',field_name='Flow per Exterior Surface Area').set(building,inf_val)
        return
    
    parameters.append(Parameter(name='Design Infiltration Rate (L/s/s-m2 @ 5Pa)',
                               selector=GenericSelector(set=litre_to_m3),
                               value_descriptor=RangeParameter(min_val=0.015,max_val=1.5))) 
    defaults.update({parameters[-1].value_descriptors[0].name:0.25})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #This parameter modifies the infiltration equation. A value of 0 indicates an entirely constant infiltration. A value of 1 indicates infiltration entirely dependent on temperature and wind.
    def inf_factors(building,value):
        const=(1-value)*1
        temp=(1-value)*0.05
        wind=(1-value)*0.3
        FieldSelector(class_name='ZoneInfiltration:DesignFlowRate',object_name='*',field_name='Constant Term Coefficient').set(building,const)
        FieldSelector(class_name='ZoneInfiltration:DesignFlowRate',object_name='*',field_name='Temperature Term Coefficient').set(building,temp)
        FieldSelector(class_name='ZoneInfiltration:DesignFlowRate',object_name='*',field_name='Velocity Term Coefficient').set(building,wind)
        return
        
    
    #This parameter modifies the constant in the infiltration equation
    # The default should be set to 1
    parameters.append(Parameter(name='Infiltration Dynamics Factor',
                               selector=GenericSelector(set=inf_factors),
                               value_descriptor=RangeParameter(min_val=0,max_val=1))) 
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),         
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
     ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Envelope parameters start at 300
    
    def Display_Order(parameter_metadata_list):
        x = 300
        for metadata_dict in parameter_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(parameter_metadata_list)
    
    return parameters, defaults, parameter_metadata_list