from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy

def loads_parameters():
    
    """
    A parameter set that modifies the process and plug loads
    """
    
    group_name="Process and Plug Loads"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    # The net peak load of plug loads throughout the office-type areas. 
    # Default should be 7.5 W/m2 per NECB. 
    parameters.append(Parameter(name='Plug Loads (W/m2)',
                               selector=FieldSelector(class_name='ElectricEquipment',
                                                      object_name='Plug Load',field_name='Watts per Zone Floor Area'),
                               value_descriptor=RangeParameter(min_val=0,max_val=15)))
    defaults.update({parameters[-1].value_descriptors[0].name:7.5})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,      
        "Displayed Order": None,         
        "Visibility": "Shown",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #We have a data center set up in the building right now. Realistically, it can be any 'electricity' process that that is assigned to one internal space and cooled either by a FCU connected to the central plant or a dedicated split unit. We may split this process load into an entirely different section. The units are in Watts, which is the peak load. It follows an always-on schedule so the peak load is equal to the average. 
    # The default should be 0 W.
    #This measure has been revised for a dedicated ITE equipment. Further research required for detailed modelling of data centres. 
          
    parameters.append(Parameter(name='IT Equipment (# of CPUs)',
                               selector=FieldSelector(class_name='ElectricEquipment:ITE:AirCooled',object_name='Data Loads',field_name='Number of Units'),
                               value_descriptor=RangeParameter(min_val=0,max_val=100)))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Information Technology (Number of Central Processing Units)",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,       
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
                      
    parameters.append(Parameter(name='Data Load Intensity (W/CPU)',
                               selector=FieldSelector(class_name='ElectricEquipment:ITE:AirCooled',object_name='Data Loads',field_name='Watts per Unit'),
                               value_descriptor=RangeParameter(min_val=0,max_val=1000)))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None, 
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)                  
    
    #DHW Type
    
    parameters.append(Parameter(name='Domestic Hot Water Fuel Type',
                               selector=FieldSelector(class_name='WaterHeater:Mixed',object_name='DHW',field_name='Heater Fuel Type'),
                               value_descriptor=CategoryParameter(['NaturalGas','Electricity'])))
    defaults.update({parameters[-1].value_descriptors[0].name:'Electricity'})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"category",
        "Onehot Grouping":'Domestic Hot Water Fuel Type',
        "Categories": ['NaturalGas','Electricity'],
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,   
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Needs documentation on conversion from peak flow rate M3/s to daily, weekly, or annual flow rates
    #Assuming standard LEED fixtures and an NECB A SHW baseline, with a SWT of 35C. Each occupant uses approx 6.65 Litres per weekday.
    #To confirm DHW consumption is at 365 m3/yr at default
    def week_to_peak(building,value):
        floorarea=4980
        occs=floorarea/(FieldSelector(class_name='People',object_name='Office Occupancy',field_name='Zone Floor Area per Person').get(building)[0])
        peak=value*occs*0.00000000582954413 #conversion from L/week/occ to m3/s peak
        FieldSelector(class_name='WaterHeater:Mixed',object_name='DHW',field_name='Peak Use Flow Rate').set(building,peak)
        return
    
    
    parameters.append(Parameter(name='Domestic Hot Water Consumption (L/week-occ @ 35C)',
                               selector=GenericSelector(set=week_to_peak),
                               value_descriptor=RangeParameter(min_val=0,max_val=50)))
    defaults.update({parameters[-1].value_descriptors[0].name:35})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #General External Gas Consumption - constant consumption 8760 hours/year
    def process_convert(building,value):
        floorarea=4980
        design=value*floorarea
        FieldSelector(class_name='Exterior:FuelEquipment',object_name='Process',field_name='Design Level').set(building,design)
        return
    parameters.append(Parameter(name='Process Gas Demand (W/m2)',
                               selector=GenericSelector(set=process_convert),
                               value_descriptor=RangeParameter(min_val=0,max_val=10)))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),    
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
        ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Loads parameters start at 500
    
    def Display_Order(parameter_metadata_list):
        x = 500
        for metadata_dict in parameter_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(parameter_metadata_list)
    
    return parameters, defaults, parameter_metadata_list