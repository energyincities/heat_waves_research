# This is an expansive set of parameters for NZN medium office


from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy


    
    


# Need to do a complex geometry set

# Need to add a way to do floor height multipliers


#         # floor multiplier = number of storeys - 2 (applied to "middle" zones)
#         def middle_zone_filter(building):
#             a = [i for i in building.idfobjects['ZONE'] if 'Middle' in i.Name]
#             return a

#     #     parameters.append(Parameter(
#     #         selector = FilterSelector(middle_zone_filter, 'Multiplier'),
#     # #         value_descriptor = RangeParameter(min_val = 0, max_val = 1)
#     #         value_descriptor = CategoryParameter(list(range(1,4,1)))
#         )


#  
def internal_load_parameters():
    """
    A parameter set that modifies the internal loads of the building - usually not directly designed, but put here as a set of 'levers' to better match buildings, or to look at sensitivity to input assumptions.
    A recent study by O'Brien et al showed a difference by an order-of-magnitude between different code-standards for these inputs based on different countries around the world. 
    """
    
    group_name="Internal Loads and Occupancy"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    
    
    

    
    


def lighting_parameters():
    """
    A parameter set that modifies overall lighting characteristics for the building
    """
    
    group_name="Lighting"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    # This is the installed lighting power density, in W/m2 of general office area.
    # 5 W/m2 is easily achievable with new LED technology. Even lower for a well-designed space. For consistency, however, we will use 10.6 W/m2 based on open office from table 4.2.1.6 of NECB 2015.
    parameters.append(Parameter(name='Overhead Lighting Power Density (W/m2)',
                               selector=FieldSelector(class_name='Lights',
                                                      object_name='Overhead Lighting',field_name='Watts per Zone Floor Area'),
                               value_descriptor=RangeParameter(min_val=0,max_val=20))) 
    defaults.update({parameters[-1].value_descriptors[0].name:10.6})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)

    #This is task lighting. It has been set up to follow the exact same schedule as the occupancy. It could also be a fudge-factor for plug loads dependent on occupancy. 
    # A personal LED task light is probably around 5W, so that would be a reasonable default, but quite often it is not installed, so we are using 0W.
    parameters.append(Parameter(name='Task Lighting Power Density (W/occ)',
                               selector=FieldSelector(class_name='Lights',
                                                      object_name='Task Lighting',field_name='Watts per Person'),
                               value_descriptor=RangeParameter(min_val=0,max_val=200))) 
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is used to modify the fraction of overhead lights that are controlled by daylight sensors. Currently task lighting occurs irrespective of daylighting.
    # This could be modified to include the tasklighting aswell. 
    # Difficult to decide on a default value here. A value of 1 would be considered all lghts in perimeter zones are controlled by the central sensors. Maybe a default of 0.5?
    parameters.append(Parameter(name='Daylighting Fraction',
                               selector=FieldSelector(class_name='Daylighting:Controls',
                                                      object_name='*',field_name='Fraction of Zone Controlled by Reference Point 1'),
                               value_descriptor=RangeParameter(min_val=0,max_val=1))) 
    defaults.update({parameters[-1].value_descriptors[0].name:.5})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This defines the setpoint for work-level lux for daylighting to maintain. 
    # A default value of 500 would be reasonable
    parameters.append(Parameter(name='Daylighting Threshold (Lux)',
                               selector=FieldSelector(class_name='Daylighting:Controls',
                                                      object_name='*',field_name='Illuminance Setpoint at Reference Point 1'),
                               value_descriptor=RangeParameter(min_val=300,max_val=1000))) 
    defaults.update({parameters[-1].value_descriptors[0].name:500})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    
    return parameters, defaults, parameter_metadata_list

    
def system_parameters():
    """
    This parameter set modifies the sizing of terminal units as a way to select HVAC system types in a continuous manner.
    Systems included:
    VAV, Chilled Beams + DOAS, FCUs + DOAS, HW Baseboards
    
    The method may be switched to a categorical parameter, either 0.001 (not installed) or 1 (installed) and then potentially a different parameter to modify the relative size (oversized, undersized, sensitivity to sizing choice)
    
    This methodology is a unique way to do large parametric sets with multiple mechanical systems in a way that is integrated into one single .idf file. It removes the need to do programatic changes to the idf file within parametrics.
    The caveat is that loads are established and dealt with in a hierarchical order:
    First, the DOAS supplies outdoor air
    Second, the chilled beam supplies ventilation air and meets whatever cooling load it can
    Third, the VAV system meets the loads,
    Fourth, the FCU meets the loads,
    Fifth, the baseboards meet the loads.
    
    Therefore, if a FCU and a VAV unit are both sized at 1, only the FCU will only meet whatever loads the VAV cannot meet. This *could* represent a type of parallel-box system setup where a separate fan kicks on to meet heating loads while the VAV is at minimum flow nad supplying required ventilation loads, for instance. 
    
    For the NZN application, however, this level of detail may be overwhelming to a novice user. 
    
    The general sizing objects may be modified so that each zone uses a sizing object for each respective mechanical system. This would be more programmatively intensive on the parametric side, but would allow for partial-installations of equipment. For example, all core-units could be VAV systems while all perimeter could be served by FCUs, etc. 
    
    
    
    The system selection is being revised to a categorical method which will be transformed into one-hot encoding for ML training.
    """
    
    #parameter list to be updated for multiple field names
    group_name="Air System Selection"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This parameter set modifies 3 fields in the VAV sizing object, for two different sets (the general sizing and the core VAV sizing). 
    #The default should be 0.001 to signify there is effectively no VAV system. 
    vav_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    vav_selectors=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General VAV Sizing',field_name=field) for field in vav_fields]
    vav_selectors.extend([FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='Core VAV Sizing',field_name=field) for field in vav_fields])
    def set_vav(building, value):
        for selector in vav_selectors: selector.set(building,value) 
        return
    
    #This modifies 3 different fields within the chilled beam sizing objects.
    #The default should be 0.001 to signify no chilled beam is installed. 
    cb_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    cb_selectors=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General CB Sizing',field_name=field) for field in cb_fields]
    def set_cb(building, value):
        for selector in cb_selectors: selector.set(building,value) 
        return    

    
    #This modifies 4 different fields within the FCU sizing object
    #A default sizing of 1.0 indicates an FCU is installed at the default autosize. 
    fcu_fields = ['Cooling Fraction of Autosized Cooling Supply Air Flow Rate','Heating Fraction of Heating Supply Air Flow Rate','Fraction of Autosized Cooling Design Capacity',
                 'Fraction of Autosized Heating Design Capacity']
    fcu_selectors=[FieldSelector(class_name='DesignSpecification:ZoneHVAC:Sizing',object_name='General FCU Sizing',field_name=field) for field in fcu_fields]
    fcu_selectors.append(FieldSelector(class_name='Schedule:Constant',object_name='FCU Sizing',field_name='Hourly Value'))
    def set_fcu(building, value):
        for selector in fcu_selectors: selector.set(building,value) 
        return     
    
    def set_system(building,value):
        if value=='VAV':
            set_vav(building,1)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
        else:
            set_vav(building,0.001)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
        if value=='FCU+DOAS':
            set_fcu(building,1)
            FieldSelector(class_name='ZoneHVAC:FourPipeFanCoil',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
            FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General DOAS Sizing',field_name='Fraction of Minimum Outdoor Air Flow').set(building,1)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:NoReheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
        else:
            set_fcu(building,0.001)
            FieldSelector(class_name='ZoneHVAC:FourPipeFanCoil',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
            FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General DOAS Sizing',field_name='Fraction of Minimum Outdoor Air Flow').set(building,.001)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:NoReheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
        if value=='Chilled Beam':
            set_cb(building,1)
            FieldSelector(class_name='AirTerminal:SingleDuct:ConstantVolume:CooledBeam',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
        else:
            set_cb(building,0.001)
            FieldSelector(class_name='AirTerminal:SingleDuct:ConstantVolume:CooledBeam',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
    
    
    parameters.append(Parameter(name='Air System Selection',
                               selector=GenericSelector(set=set_system),
                               value_descriptors=CategoryParameter(['VAV','FCU+DOAS','Chilled Beam'])))
    defaults.update({parameters[-1].value_descriptors[0].name:'FCU+DOAS'})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"category",
        "Onehot Grouping":"Air System Selection",
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
     
    #This sizes the baseboard terminals.
    # A default size of 0.001 indicates that hot water terminals are not installed.
    # A future potential would be to include electric baseboards.
    parameters.append(Parameter(name='Baseboard Sizing',
                               selector=FieldSelector(class_name='ZoneHVAC:Baseboard:RadiantConvective:Water',
                                                      object_name='*',field_name='Fraction of Autosized Heating Design Capacity'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    return parameters, defaults, parameter_metadata_list


def FCU_parameters():
    """
    This set of parameters modifies factors that relate to the fan-coil unit system type
    Potential parameters to investigate adding:
    - Constant volume fans
    - Different OAT temperature points for reset
    
    The FCU parameters assume the DOAS is installed. The DOAS includes a preheat coil, an ERV, a heating coil, and a cooling coil.
    The DOAS currently assumes the preheat coil will heat the OA to -20C as a form of freeze protection for the ERV.
    The freeze protection setpoint as well as preheat coil setpoint should be added as two variables in the future. 
    The energyplus IOReference gives good defaults for freeze protection. 
    
    The economizer/heat recovery bypass controls need to be verified.
    """

    group_name="Fan Coil Unit + Dedicated Outdoor Air System"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This modifies 4 different fields within the FCU sizing object
    #A default sizing of 1.0 indicates an FCU is installed at the default autosize. 
    fcu_fields = ['Cooling Fraction of Autosized Cooling Supply Air Flow Rate','Heating Fraction of Heating Supply Air Flow Rate','Fraction of Autosized Cooling Design Capacity',
                 'Fraction of Autosized Heating Design Capacity']
    fcu_selectors=[FieldSelector(class_name='DesignSpecification:ZoneHVAC:Sizing',object_name='General FCU Sizing',field_name=field) for field in fcu_fields]
    fcu_selectors.append(FieldSelector(class_name='Schedule:Constant',object_name='FCU Sizing',field_name='Hourly Value'))
    def set_fcu(building, value):
        for selector in fcu_selectors: selector.set(building,selector.get(building)[0]*value) 
        return   
    
    parameters.append(Parameter(name='FCU Sizing Factor',
                               selector=GenericSelector(set=set_fcu),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
        
    # The fan pressure, including both internal and external static pressure, is the dominant variable dictating FCU fan energy
    # Default value of 125 Pa, or a half inch of TSP is reasonable for a FCU. 
    parameters.append(Parameter(name='Terminal Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:SystemModel',
                                                      object_name='*',field_name='Design Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=750)))
    defaults.update({parameters[-1].value_descriptors[0].name:125})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The DOAS supply fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 1000 Pa, or 4 inches of static.
    parameters.append(Parameter(name='DOAS Supply Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS Supply Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))    
    defaults.update({parameters[-1].value_descriptors[0].name:1000})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The DOAS exhaust fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 750 Pa, or 3 inches of static.
    parameters.append(Parameter(name='DOAS Exhaust Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS Return Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:750})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #These sets of fields change the sensible effectiveness of the installed ERV. Here we assume that all sensible effectiveness is equal regardless of flow rate - a liberal assumption
    # A default value of 0.70 could represent a typical heat wheel. 
    erv_sen_fields = ['Sensible Effectiveness at 100 Heating Air Flow','Sensible Effectiveness at 75 Heating Air Flow','Sensible Effectiveness at 100 Cooling Air Flow',
                      'Sensible Effectiveness at 75 Cooling Air Flow']
    erv_sen_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS Heat Recovery',field_name=field) for field in erv_sen_fields]
    def set_erv_sen(building, value):
        for selector in erv_sen_selectors: selector.set(building,value) 
        return      
       
    parameters.append(Parameter(name='ERV Sensible Effectiveness',
                               selector=GenericSelector(set=set_erv_sen),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.7})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #These sets of fields change the latent effectiveness of the installed ERV. Here we assume that all latent effectiveness is equal regardless of flow rate - a liberal assumption
    #This variable should be modified to a fraction of sensible effectiveness. 
    # A default value of 0.50 could represent a typical heat wheel. 
    erv_lat_fields = ['Latent Effectiveness at 100 Heating Air Flow','Latent Effectiveness at 75 Heating Air Flow','Latent Effectiveness at 100 Cooling Air Flow',
                      'Latent Effectiveness at 75 Cooling Air Flow']
    erv_lat_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS Heat Recovery',field_name=field) for field in erv_lat_fields]
    def set_erv_lat(building, value):
        for selector in erv_lat_selectors: selector.set(building,value) 
        return      
       
    parameters.append(Parameter(name='ERV Latent Effectiveness',
                               selector=GenericSelector(set=set_erv_lat),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.5})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is an outdoor air temperature reset that modifies the setpoint that the ERV & Cooling coil attempt to maintain. The heating coil only maintains 11-12C.
    #OAT at low is 13C (could be modified later)
    #A default value could be 18C - a slightly cool supply temperature. Default revised to 21 after comparing internal cooling loads.
    parameters.append(Parameter(name='DOAS Low SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor Low Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:21})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #OAT at high is 23C (could be modified later)
    #A default value could be 18C - a slightly cool supply temperature - revised to 21C
    parameters.append(Parameter(name='DOAS High SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor High Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:21})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata) 
      
    

    return parameters, defaults, parameter_metadata_list

def VAV_parameters():
    """
    This set of parameters modifies factors that relate to the VAV unit system type. Currently the VAV boxes are reheat boxes with a minimum fraction of air flow when occupied. 
    Parameters to add:
    - Sizing temperatures to size to the actual lowest supply temperature. New high-performance VAV system recommendations use 10C as the design temperature. 
    - Choice between fraction vs. constant flow minimum setpoints in boxes
    - Potential for sizing minimum based on other factors (such as optimal ventilation)
    """

    group_name="Built-up VAV"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This parameter set modifies 3 fields in the VAV sizing object, for two different sets (the general sizing and the core VAV sizing). 
    #The default should be 0.001 to signify there is effectively no VAV system. 
    vav_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    vav_selectors_per=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General VAV Sizing',field_name=field) for field in vav_fields]
    def set_vav_perimeter(building, value):
        for selector in vav_selectors_per: selector.set(building,selector.get(building)[0]*value) 
        return
    
    parameters.append(Parameter(name='VAV Perimeter Sizing Factor',
                               selector=GenericSelector(set=set_vav_perimeter),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This parameter only modifies core zone VAV systems sizing. Oversizing core units is done because they are typically in cooling year-round. A temperature strategy like above will be dominated by the core system, and oversizing the box can allow the system to turn-down even more.
    #A default value of 0.001 would be no core VAV terminal. 
    vav_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    vav_selectors_core=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='Core VAV Sizing',field_name=field) for field in vav_fields]
    def set_vav_core(building, value):
        for selector in vav_selectors_core: selector.set(building,selector.get(building)[0]*value) 
        return
    
    parameters.append(Parameter(name='VAV Core Sizing Factor',
                               selector=GenericSelector(set=set_vav_core),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #The minimum box position of the VAV when cooling is not required. Recent ASHRAE and DoE recent highlights that historic rules-of-thumb for minimum requirements tend to be oversized due to fears of improper air mixing, which can have a huge effect on energy consumption for reheat. 
    # default of 0.30 is reasonable. 
    parameters.append(Parameter(name='Minimum Box Position',
                               selector=FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',
                                                      object_name='*',field_name='Constant Minimum Air Flow Fraction'),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.3})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The VAV boxes are set as reheat boxes, where airflow is allowed to increase in heating if coil is at maximum position.
    # Default value of 0.30 means it cannot go beyond minimum flow. 
    parameters.append(Parameter(name='Maximum Reheat Position',
                               selector=FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',
                                                      object_name='*',field_name='Maximum Flow Fraction During Reheat'),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.3})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This assumes a 'warmest' reset, where the temperature meets the cooling demands of the warmest zone (at max flow)
    # A default value of 13 for standard VAVs. 
    parameters.append(Parameter(name='VAV SAT Reset Low (C)',
                               selector=FieldSelector(class_name='SetpointManager:Warmest',
                                                      object_name='Central VAV Cooling Supply Air Temp Manager',field_name='Minimum Setpoint Temperature'),
                               value_descriptor=RangeParameter(min_val=10,max_val=13)))
    defaults.update({parameters[-1].value_descriptors[0].name:13})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This assumes a 'warmest' reset, where the temperature meets the cooling demands of the warmest zone (at max flow)
    # A maximum is entered of 21C to prevent the VAV system from breaking at temperatures that are too warm.
    
    def vav_reset_high(building,value):
        min_temp=FieldSelector(class_name='SetpointManager:Warmest',
                                                      object_name='Central VAV Cooling Supply Air Temp Manager',field_name='Minimum Setpoint Temperature').get(building)
        max_temp=min(min_temp[0]+value,21)
        FieldSelector(class_name='SetpointManager:Warmest',
                                                      object_name='Central VAV Cooling Supply Air Temp Manager',field_name='Maximum Setpoint Temperature').set(building,max_temp)
    
    parameters.append(Parameter(name='VAV SAT Reset High (C)',
                               selector=GenericSelector(set=vav_reset_high),
                               value_descriptor=RangeParameter(min_val=0,max_val=7)))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
      
    
    #Modify pressure of central fan as way of controlling supply fan power
    parameters.append(Parameter(name='VAV Supply Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='Central VAV Supply Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=10,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:1000})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Modify pressure of central fan as way of controlling return fan power
    parameters.append(Parameter(name='VAV Return Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='Central VAV Return Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=10,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:250})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    

    

    return parameters, defaults, parameter_metadata_list

def CB_parameters():
    """
    This set of parameters modifies factors that relate to the chilled beam unit system type
    Parameters to add:
    - DOAS temperature control
    - DOAS dessicant
    - DOAS variable air-side flow rate
    - Convert DOAS to mixed-air system to induce zone-air and/or humidity control without additional OA
    - Ventilation rate based on a per-floor area flow rate. Active chilled beam sizing parameters are a balance between induction flow rate and the number of chilled beams installed, effectively balancing capital cost and energy consumption. 
    """

    group_name="Chilled Beam"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This modifies 3 different fields within the chilled beam sizing objects.
    #The default should be 0.001 to signify no chilled beam is installed. 
    cb_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    cb_selectors=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General CB Sizing',field_name=field) for field in cb_fields]
    def set_cb(building, value):
        for selector in cb_selectors: selector.set(building,selector.get(building)[0]*value) 
        return  
    
    parameters.append(Parameter(name='Chilled Beam Sizing Factor',
                               selector=GenericSelector(set=set_cb),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    

    # The DOAS supply fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 1000 Pa, or 4 inches of static.
    parameters.append(Parameter(name='DOAS CB Supply Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS CB Supply Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))    
    defaults.update({parameters[-1].value_descriptors[0].name:1000})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The DOAS exhaust fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 750 Pa, or 3 inches of static.
    parameters.append(Parameter(name='DOAS CB Exhaust Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS CB Return Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:750})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)

    #CB ERV TBD
#     erv_sen_fields = ['Sensible Effectiveness at 100% Heating Air Flow','Sensible Effectiveness at 75% Heating Air Flow','Sensible Effectiveness at 100% Cooling Air Flow',
#                       'Sensible Effectiveness at 75% Cooling Air Flow']
#     erv_sen_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS CB Heat Recovery',field_name=field) for field in erv_sen_fields]
#     def set_erv_sen(building, value):
#         for selector in erv_sen_selectors: selector.set(building,value) 
#         return      

#     parameters.append(Parameter(name='ERV Sensible Effectiveness',
#                                selector=GenericSelector(set=set_erv_sen),
#                                value_descriptor=RangeParameter(min_val=0,max_val=1)))

#     erv_lat_fields = ['Latent Effectiveness at 100% Heating Air Flow','Latent Effectiveness at 75% Heating Air Flow','Latent Effectiveness at 100% Cooling Air Flow',
#                       'Latent Effectiveness at 75% Cooling Air Flow']
#     erv_lat_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS CB Heat Recovery',field_name=field) for field in erv_lat_fields]
#     def set_erv_lat(building, value):
#         for selector in erv_lat_selectors: selector.set(building,value) 
#         return      

#     parameters.append(Parameter(name='ERV Latent Effectiveness',
#                                selector=GenericSelector(set=set_erv_lat),
#                                value_descriptor=RangeParameter(min_val=0,max_val=1)))

    #This is an outdoor air temperature reset that modifies the setpoint that the ERV & Cooling coil attempt to maintain. The heating coil only maintains 11-12C.
    #OAT at low is 13C (could be modified later)
    # Default of 13C for humidity control for now
    parameters.append(Parameter(name='DOAS CB Low SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS CB Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor Low Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:13})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #OAT at high is 23C (could be modified later)
    # Default of 13C for humitiy control for now
    parameters.append(Parameter(name='DOAS CB High SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS CB Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor High Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:13})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    

    return parameters, defaults, parameter_metadata_list

def heating_plant_parameters():
    """
    This set of parameters modifies the selection and design of the heating equipment
    
    In order to demand flow through some plant equipment, the plant has been set up as primary/secondary with a common pipe.
    The primary flow is constant volume. The secondary flow is variable volume.
    There is a full-sized boiler and chiller at bottom priority on each primary loop to request flow whenever there is a load, otherwise the ASHP (etc) may not recognize and demand flow.
    The plant loop equipment is set up in a hierarchical manner
    
    For heating:
    First, the ASHP meets whatever load it can
    Second, the first hot water NG boiler,
    Third, the second hot water NG boiler,
    Fourth, the load is met by a district heating systems
    Fifth, an electric boiler is there to demand flow, but currently will not see any load after the district
    """
    group_name="Heating Plant Design"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #Boiler-1 and boiler-2 are both condensing natural gas boilers. Nominal efficiency is measured at 82C temp (non-condensing range)
    #The relative size of boiler-1(NG). This should be kept as a continuous variable.
    # A reasonable default would be 0.5, or sized at 50% of the peak load.
    
    def gas_boiler_quantity(building,value):

        if value==0:
            boiler_1=0.00001
            boiler_2=0.00001
        if value==1:
            boiler_1=1
            boiler_2=0.00001
        else:
            boiler_1=1
            boiler_2=1

        FieldSelector(class_name='Boiler:HotWater',object_name='Boiler-1',field_name='Maximum Part Load Ratio').set(building,boiler_1)
        FieldSelector(class_name='Boiler:HotWater',object_name='Boiler-2',field_name='Maximum Part Load Ratio').set(building,boiler_2)
    
    
    parameters.append(Parameter(name='Gas Boiler Quantity',
                               selector=GenericSelector(set=gas_boiler_quantity),
                               value_descriptor=CategoryParameter([0,1,2])))
    defaults.update({parameters[-1].value_descriptors[0].name:2})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"category",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)    
    
    
    
    
    #Boiler-1 and boiler-2 are both condensing natural gas boilers. Nominal efficiency is measured at 82C temp (non-condensing range)
    #The relative size of boiler-1(NG). This should be kept as a continuous variable.
    # A reasonable default would be 0.5, or sized at 50% of the peak load.
    parameters.append(Parameter(name='Boiler-1 Sizing',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-1',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.2,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.5})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":[1,2]
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #The relative size of boiler-2(NG). This should be kept as a continuous variable.
    # A reasonable default would be 0.5, or sized at 50% of the peak load.
    parameters.append(Parameter(name='Boiler-2 Sizing',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-2',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.2,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.5})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":[2]
    })
    parameter_metadata_list.append(parameter_metadata)
    
        #Nominal boiler efficiency. Nominal efficiency is at full-load conditions and at a SWT of 82C. It assumes that all boilers are condensing, but that condensing only occurs at the proper temperatures. A high-efficiency condensing boiler will have a nominal efficiency of 86% at these conditions. It may be possible at very low conditions that efficiency may be above 100% for short periods of low-load.
    
    #Default thermal efficiency of 0.82 (or 82%) would results in a peak efficiency of 96% or so.
    parameters.append(Parameter(name='Boiler-1 Nominal Efficiency (%)',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-1',field_name='Nominal Thermal Efficiency'),
                               value_descriptor=RangeParameter(min_val=.5,max_val=.88)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.82})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":[1,2]
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Default thermal efficiency of 0.82 would results in a peak efficiency of 96% or so.
    parameters.append(Parameter(name='Boiler-2 Nominal Efficiency (%)',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-2',field_name='Nominal Thermal Efficiency'),
                               value_descriptor=RangeParameter(min_val=.5,max_val=.88)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.82})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":[2]
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #ASHP is separated into heating and cooling. They are not connected.
    #Default value of 0.001 would be no ASHP for heating
    #Adding a conditional, where if the sizing is very small the ASHPs are not activated.
    
    
    def ashp_heater_enable(building,value):
       
        if value<.9:
            eqptlist_heater='All Heaters-2'
        else:
            eqptlist_heater='All Heaters'
           
        FieldSelector(class_name='PlantEquipmentOperation:HeatingLoad',object_name='HWL-1 Operation All Hours',field_name='Range 1 Equipment List Name').set(building,eqptlist_heater)
        return
        
    
    parameters.append(Parameter(name='ASHP Heating Enable',
                               selector=GenericSelector(set=ashp_heater_enable),
                               value_descriptor=CategoryParameter([0,1])))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"boolean",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    parameters.append(Parameter(name='ASHP Heating Sizing',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Heating',object_name='ASHP Heating',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.001,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Heating Enable",
        "Dependent Parameter Value":[1]
    })
    parameter_metadata_list.append(parameter_metadata)
    
        
    #ASHP Heating efficiency is determined at an OAT of 7C and a SWT of 45C at full-load. It is modelled after an AERMEC NRL 280-750 (HA-HE version) with nominal heating COP of approximately 3
    #Default value of 3 is reasonable. 
    parameters.append(Parameter(name='ASHP Heating Efficiency (COP)',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Heating',
                                                      object_name='ASHP Heating',field_name='Reference Coefficient of Performance'),
                               value_descriptor=RangeParameter(min_val=2,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:3})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Heating Enable",
        "Dependent Parameter Value":[1]
    })
    parameter_metadata_list.append(parameter_metadata)
                      
      #These factors are used to size the flow rate in the plant loops
    #Default of 20C would be reasonable for a high-performance hot water loop
    parameters.append(Parameter(name='Hot Water DeltaT (C)',
                               selector=FieldSelector(class_name='Sizing:Plant',
                                                      object_name='HWL-1 Hot Water Loop',field_name='Loop Design Temperature Difference'),
                               value_descriptor=RangeParameter(min_val=5,max_val=25)))
    defaults.update({parameters[-1].value_descriptors[0].name:20})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
                      
    #These parameters will alter the supply water temperature for the heating, loops as well as the design temperature
    
    #Hot Water Loop
    #Low OAT is at -25 for the HWL and -10 for the ASHP outlet, which is an equipment limit
    #Default of 60C for a low-temperature HWL
    HW_low_selectors=[]
    HW_low_selectors.append(FieldSelector(class_name='Sizing:Plant',
                                                      object_name='HWL-1 Hot Water Loop',field_name='Design Loop Exit Temperature'))    
    HW_low_selectors.append(FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='HWL-1 HW Temp Manager',field_name='Setpoint at Outdoor Low Temperature'))
    
    def set_HW_low(building, value):
        for selector in HW_low_selectors: selector.set(building,value) 
        return  
    
    parameters.append(Parameter(name='Hot Water Low Temp (C)',
                               selector=GenericSelector(set=set_HW_low),
                               value_descriptor=RangeParameter(min_val=45,max_val=82)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Reset temperature at warm temperature, currently set at 15C OAT
    #Default of 40C for a low-temperature HWL 
    parameters.append(Parameter(name='Hot Water High Temp (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='HWL-1 HW Temp Manager',field_name='Setpoint at Outdoor High Temperature'),
                               value_descriptor=RangeParameter(min_val=30,max_val=82)))
    defaults.update({parameters[-1].value_descriptors[0].name:40})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)    
                      



    #Hot water supply pump head. Min - 20 ft, max - 90 ft
    #Assume default of 60 ft
    def set_HW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='HWL-1 HW Demand Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3) 
        return  
    parameters.append(Parameter(name='Hot Water Head (ft)',
                               selector=GenericSelector(set=set_HW_pump),
                               value_descriptor=RangeParameter(min_val=20,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)    
    
    #Default will be 0.001 to ignore the pump.
    def set_prim_HW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='HWL-1 HW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)     
        return  
    parameters.append(Parameter(name='Primary Hot Water Head (ft)',
                               selector=GenericSelector(set=set_prim_HW_pump),
                               value_descriptor=RangeParameter(min_val=.001,max_val=60)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    return parameters, defaults, parameter_metadata_list
    
    
    

def cooling_plant_parameters():
    """
    This set of parameters modifies the selection of plant equipment
    
    In order to demand flow through some plant equipment, the plant has been set up as primary/secondary with a common pipe.
    The primary flow is constant volume. The secondary flow is variable volume.
    There is a full-sized boiler and chiller at bottom priority on each primary loop to request flow whenever there is a load, otherwise the ASHP (etc) may not recognize and demand flow.
    The plant loop equipment is set up in a hierarchical manner
        
    For cooling:
    First, the water-side cooling meets whatever load it can.
    Second, the ASHP meets whatever load it can
    Third, the first water-cooled chiller,
    Fourth, the second water-cooled chiller,
    Fifth, the first air-cooled chiller,
    Sixth, the second air-cooled chiller,
    Seventh, the load is met by a district cooling systems
    Eighth, an air-cooled chiller is there to demand flow, but currently will not see any load after the district
    
    This ordering will be updated with further equipment.
    There is also a combination of series vs. parallel equipment.
    
    The ASHP currently meets as much load as it can before branching into the other equipment depending on where on the load list it is. Therefore, the temperature entering the branched equipment will be slightly-conditioned.
    As more equipment is added it will either be set in series w/ the ASHP or in parallel with the main equipment.
    
    """
    
    group_name="Cooling Plant Design"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    
    chiller_type=CategoryParameter(['Air-Cooled','Water-Cooled','Mixed'],name='Chiller Type')
    chiller_number=CategoryParameter([0,1,2],name='Chiller Quantity')
    chiller_1_sizing=RangeParameter(min_val=0.1,max_val=1.3,name='Chiller-1 Size')
    chiller_2_sizing=RangeParameter(min_val=0.1,max_val=1.3,name='Chiller-2 Size')
    chiller_1_eff=RangeParameter(min_val=1,max_val=8,name='Chiller-1 Efficiency')
    chiller_2_eff=RangeParameter(min_val=1,max_val=8,name='Chiller-2 Efficiency')

    def chiller_design(building,chiller_type,chiller_number,chiller_1_sizing,chiller_2_sizing,chiller_1_eff,chiller_2_eff):
        chiller_1_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-1',field_name='Sizing Factor')
        chiller_1_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-1',field_name='Reference COP')
        chiller_2_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-2',field_name='Sizing Factor')
        chiller_2_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-2',field_name='Reference COP')
        chiller_3_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-3',field_name='Sizing Factor')
        chiller_3_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-3',field_name='Reference COP')
        chiller_4_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-4',field_name='Sizing Factor')
        chiller_4_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-4',field_name='Reference COP')

        chiller_1_sel.set(building,0.0001)
        chiller_2_sel.set(building,0.0001)
        chiller_3_sel.set(building,0.0001)
        chiller_4_sel.set(building,0.0001)

        if chiller_type=='Air-Cooled':
            chiller_sels=[chiller_3_sel,chiller_4_sel]
            chiller_effs=[chiller_3_sel_eff,chiller_4_sel_eff]
        if chiller_type=='Water-Cooled':
            chiller_sels=[chiller_1_sel,chiller_2_sel]
            chiller_effs=[chiller_1_sel_eff,chiller_2_sel_eff]        
        if chiller_type=='Mixed':
            chiller_sels=[chiller_3_sel,chiller_1_sel]
            chiller_effs=[chiller_3_sel_eff,chiller_1_sel_eff]        

        if chiller_number==0:
            return
        if chiller_number==1:
            chiller_sels[0].set(building,chiller_1_sizing)
            chiller_effs[0].set(building,chiller_1_eff)
        if chiller_number==2:
            chiller_sels[0].set(building,chiller_1_sizing)
            chiller_sels[1].set(building,chiller_2_sizing)
            chiller_effs[0].set(building,chiller_1_eff)
            chiller_effs[1].set(building,chiller_2_eff)

        return

    parameters.append(Parameter(
        selector=GenericSelector(set = chiller_design),
        # the values from these descriptors are passed to the function in order
        value_descriptors=[chiller_type,chiller_number,chiller_1_sizing,chiller_2_sizing,chiller_1_eff,chiller_2_eff]
        ))

    defaults_list=['Air-Cooled',2,.5,.5,3,3]
    onehot_list=['Chiller Type',None,None,None,None,None]
    dep_par_list=[None,None,'Chiller Quantity','Chiller Quantity','Chiller Quantity','Chiller Quantity']
    dep_val_list=[None,None,[1,2],[2],[1,2],[2]]
    for i in range(len(parameters[-1].value_descriptors)):
        defaults.update({parameters[-1].value_descriptors[i].name:defaults_list[i]})
        parameter_metadata=({
            "label":parameters[-1].value_descriptors[i].name,
            "Min":parameters[-1].value_descriptors[i].min if isinstance(parameters[-1].value_descriptors[i],RangeParameter) else None,
            "Max":parameters[-1].value_descriptors[i].max if isinstance(parameters[-1].value_descriptors[i],RangeParameter) else None,
            "Default":defaults.get(parameters[-1].value_descriptors[i].name),
            "Parameter Group":group_name,
            "Type":"continuous" if isinstance(parameters[-1].value_descriptors[i],RangeParameter) else "boolean" if parameters[-1].value_descriptors[1].options==[0,1] else "category",
            "Onehot Grouping":onehot_list[i],
            "Dependent Parameter":dep_par_list[i],
            "Dependent Parameter Value":dep_val_list[i]
        })
        parameter_metadata_list.append(parameter_metadata)    
    
    
    
    
    
    
    #Default value of 0.001 would be no ASHP for cooling
    
    def ashp_chiller_enable(building,value):
        if value<0.9:
            eqptlist_chiller='All Chillers-2'
        else:
            eqptlist_chiller='All Chillers'
           
        FieldSelector(class_name='PlantEquipmentOperation:CoolingLoad',object_name='ChW-1 Chiller Operation All Hours',field_name='Range 1 Equipment List Name').set(building,eqptlist_chiller)
        return
    
    
    parameters.append(Parameter(name='ASHP Cooling Enable',
                               selector=GenericSelector(set=ashp_chiller_enable),
                               value_descriptor=CategoryParameter([0,1])))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"category",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    parameters.append(Parameter(name='ASHP Cooling Sizing',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Cooling',object_name='ASHP Cooling',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.001,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Cooling Enable",
        "Dependent Parameter Value":1
    })
    parameter_metadata_list.append(parameter_metadata)
    
        #ASHP Cooling efficiency is determined at an OAT of 35C and a SWT of 6.7C at full-load. It is modelled after an AERMEC NRL 280-750 (HA-HE version) with nominal cooling COP of approximately 3
    #Default value of 3 is reasonable. 
    parameters.append(Parameter(name='ASHP Cooling Efficiency (COP)',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Cooling',
                                                      object_name='ASHP Cooling',field_name='Reference Coefficient of Performance'),
                               value_descriptor=RangeParameter(min_val=2,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:3})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Cooling Enable",
        "Dependent Parameter Value":1
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    
    #Water-side economizer is a heat exchanger between a dry-cooler enabled condenser loop at the cooling loop
    #Default value of 0.001 would be no water-side economizer
    parameters.append(Parameter(name='Waterside Economizer Sizing',
                               selector=FieldSelector(class_name='HeatExchanger:FluidToFluid',
                                                      object_name='Waterside Economizer',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.001,max_val=1.5)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    

    #Default of 6C would be reasonable. 
    parameters.append(Parameter(name='Chilled Water DeltaT (C)',
                               selector=FieldSelector(class_name='Sizing:Plant',
                                                      object_name='ChW-1 Chilled Water Loop',field_name='Loop Design Temperature Difference'),
                               value_descriptor=RangeParameter(min_val=3,max_val=10)))
    defaults.update({parameters[-1].value_descriptors[0].name:6})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Default of 4 is reasonable. 
    parameters.append(Parameter(name='Condenser Water DeltaT (C)',
                               selector=FieldSelector(class_name='Sizing:Plant',
                                                      object_name='ChW-1 Condenser Water Loop',field_name='Loop Design Temperature Difference'),
                               value_descriptor=RangeParameter(min_val=3,max_val=10)))
    defaults.update({parameters[-1].value_descriptors[0].name:4})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    

    
    #Chilled Water Loop
    #High OAT is at 20C. This may need to be modified if humidity control requires lower temperature
    #Defualt of 7 for a typical chilled water loop. 
    ChW_high_selectors=[]
    ChW_high_selectors.append(FieldSelector(class_name='Sizing:Plant',
                                                      object_name='ChW-1 Chilled Water Loop',field_name='Design Loop Exit Temperature'))    
    ChW_high_selectors.append(FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='ChW-1 Temp Manager',field_name='Setpoint at Outdoor High Temperature'))
    
    def set_ChW_high(building, value):
        for selector in ChW_high_selectors: selector.set(building,value) 
        return  
    
    parameters.append(Parameter(name='Chilled Water High Temp (C)',
                               selector=GenericSelector(set=set_ChW_high),
                               value_descriptor=RangeParameter(min_val=4,max_val=8)))
    defaults.update({parameters[-1].value_descriptors[0].name:7})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Reset temperature at warm temperature, currently set at 12C
    # Default of 7C, or no reset. 
    parameters.append(Parameter(name='Chilled Water Low Temp (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='ChW-1 Temp Manager',field_name='Setpoint at Outdoor Low Temperature'),
                               value_descriptor=RangeParameter(min_val=4,max_val=12)))
    defaults.update({parameters[-1].value_descriptors[0].name:7})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Condenser Water Loop
    #The condenser water loop is controlled via EMS to follow the outside air wetbulb temperature with a deadband and limited by a minimum lift limit. The lift is measured as the difference between chilled water supply setpoint and condenser water return setpoint
    #A default minimum lift would be around 4C, as a reasonable number. 
    parameters.append(Parameter(name='Condenser Minimum Lift (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='Lift Min',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=0,max_val=20)))
    defaults.update({parameters[-1].value_descriptors[0].name:4})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #A reasonable offset would be to 2C. 
    parameters.append(Parameter(name='Condenser Setpoint WB Offset (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='CT WB Offset',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=0,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:2})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #The condenser loop can be disabled at different outdoor air temperatures to disable winter operation.This may break the data center cooling though.
    # Disable to cooling towers at -40 for default.  
    parameters.append(Parameter(name='Cooling Tower Loop Disable Temperature (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='CT Disable',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=-40,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:-40})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #The free cooling loop can be enabled at different outdoor air temperatures to enable winter operation
    # The default would be no free cooling installed, so we can set this default higher to something like 4C. 
    parameters.append(Parameter(name='Free Cooling Loop Enabled Temperature (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='Free Cool Enable',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=-20,max_val=15)))
    defaults.update({parameters[-1].value_descriptors[0].name:4})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    

    

    
    
    
    
    #Chilled water supply pump head. Min - 20 ft, max - 90 ft. It assumes that both the beam loop and secondary chilled water loop are equivalent.
    #Assume default of 60 ft
    def set_ChW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='ChW-1 ChW Demand Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='Beam ChW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)        
        return  
    parameters.append(Parameter(name='Chilled Water Head (ft)',
                               selector=GenericSelector(set=set_ChW_pump),
                               value_descriptor=RangeParameter(min_val=20,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    

    
    #Condenser water supply pump head. Min - 20 ft, max - 90 ft
    #Assume default of 60 ft
    def set_CndW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='ChW-1 CndW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3) 
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='Free Cooling CndW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3) 
        return  
    parameters.append(Parameter(name='Condenser Water Head (ft)',
                               selector=GenericSelector(set=set_CndW_pump),
                               value_descriptor=RangeParameter(min_val=20,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #Primary pump parameters, which are set to always constant. 
    #Default will be 0.001 to ignore the pump.
    def set_prim_ChW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='ChW-1 ChW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)     
        return  
    parameters.append(Parameter(name='Primary Chilled Water Head (ft)',
                               selector=GenericSelector(set=set_prim_ChW_pump),
                               value_descriptor=RangeParameter(min_val=.001,max_val=60)))
    defaults.update({parameters[-1].value_descriptors[0].name:.001})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    return parameters, defaults, parameter_metadata_list
    

def renewable_parameters():
    """
    This set of parameters modifies the renewable energy generation and storage. Currently the only parameter is PV generation. Future features to include batteries and grid integration.
    """
    
    group_name="Renewables"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #Modify nominal kW of PV
    
    def set_w_to_wpm2(building, value):
        selector = FieldSelector(class_name='Generator:PVWatts', object_name='Solar PV',field_name='DC System Capacity')
        roofarea=4980/3
        selector.set(building,value*roofarea)     
        return
    
    parameters.append(Parameter(name='PV Capacity (Wp/m2(roof))',
                               selector=GenericSelector(set=set_w_to_kw),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=500)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    parameters.append(Parameter(name='PV Tilt (degrees from horizontal)',
                               selector=FieldSelector(class_name='Generator:PVWatts', object_name='Solar PV',field_name='Tilt Angle'),
                               value_descriptor=RangeParameter(min_val=0,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:30})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    parameters.append(Parameter(name='PV Azimuth (degrees from north)',
                               selector=FieldSelector(class_name='Generator:PVWatts', object_name='Solar PV',field_name='Tilt Angle'),
                               value_descriptor=RangeParameter(min_val=90,max_val=270)))
    defaults.update({parameters[-1].value_descriptors[0].name:180})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    return parameters, defaults, parameter_metadata_list