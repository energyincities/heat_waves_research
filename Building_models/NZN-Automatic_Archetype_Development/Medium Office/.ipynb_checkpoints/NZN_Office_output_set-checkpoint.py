"""
This python file will document some combinations of meter outputs from an idf file.

To add:
- detailed breakdown (by equipment type)
- additional details like sizing
"""

def fuel_use():
    
    outputs=[]
    
    outputs.append('Electricity:Facility')
    
    outputs.append('Gas:Facility')
    
    outputs.append('DistrictHeating:Facility')
    
    outputs.append('DistrictCooling:Facility')
    
    return outputs

def metrics():
    
    outputs=[]
    
    outputs.append('TED')
    
    outputs.append('TEU')
    
    return outputs

def end_use_breakdown():
    
    outputs=[]
    
    outputs.append('Heating:Electricity')
    
    outputs.append('Heating:Gas')
    
    outputs.append('Heating:DistrictHeating')
    
    outputs.append('Cooling:Electricity')
    
    outputs.append('Cooling:DistrictCooling')
    
    outputs.append('InteriorLights:Electricity')
    
    outputs.append('ExteriorLights:Electricity')
    
    outputs.append('InteriorEquipment:Electricity')
    
    outputs.append('InteriorEquipment:Gas')
    
    outputs.append('ExteriorEquipment:Electricity')
    
    outputs.append('ExteriorEquipment:Gas')
    
    outputs.append('Fans:Electricity')
    
    outputs.append('Pumps:Electricity')
    
    outputs.append('HeatRejection:Electricity')
    
    outputs.append('Humidifier:Electricity')
    
    outputs.append('Humidifier:Gas')
    
    outputs.append('WaterSystems:Electricity')
    
    outputs.append('WaterSystems:Gas')
    
    return outputs

def renewables():
    
    outputs=[]
    
    outputs.append('Photovoltaic:ElectricityProduced')
    
    outputs.append('ElectricitySurplusSold:Facility')
    
    return outputs

# def unmet_hours():
    
#     outputs=[]
    
#     outputs.append
    
#     return outputs

    
