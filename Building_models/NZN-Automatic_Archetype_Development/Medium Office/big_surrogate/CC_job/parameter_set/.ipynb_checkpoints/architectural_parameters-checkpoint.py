from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy

def architectural_parameters():
    
    """
    A parameter set that modifies the architectural parameters of the archetype buildings
    """
    
    group_name="Architectural"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #Modify WWR. This removes all windows in the building and adds in an equivalent WWR for each surface in the building.
    # I would say the default should be 0.40
    
    def north_wwr(building, value):
        building.remove_windows()
        building.set_wwr(wwr=value,construction="Exterior Window",force=True,orientation='north')
        return
    
    parameters.append(Parameter(name='North Window to Wall Ratio (%)',
                               selector=GenericSelector(set=north_wwr),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=.99)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.40})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    def east_wwr(building, value):
        building.set_wwr(wwr=value,construction="Exterior Window",force=True,orientation='east')
        return
    
    parameters.append(Parameter(name='East Window to Wall Ratio (%)',
                               selector=GenericSelector(set=east_wwr),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=.99)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.40})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    def south_wwr(building, value):
        building.set_wwr(wwr=value,construction="Exterior Window",force=True,orientation='south')
        return
    
    parameters.append(Parameter(name='South Window to Wall Ratio (%)',
                               selector=GenericSelector(set=south_wwr),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=.99)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.40})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    def west_wwr(building, value):
        building.set_wwr(wwr=value,construction="Exterior Window",force=True,orientation='west')
        return
    
    parameters.append(Parameter(name='West Window to Wall Ratio (%)',
                               selector=GenericSelector(set=west_wwr),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=.99)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.40})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
       
    
    
    
    #Modify shading overhang length. Currently this is absolute (length of overhang in m), but in the future should be switched to Shading:Overhang:Projection so that it is relative. 
    # Since we cannot do no shading, I would say the default shoulbe 0.01. 
     
    
    
    def north_shading(building, value):
        building.remove_shading()
        building.add_overhangs(depth = value, tilt = 90, orientation = "north")
        return
    
    parameters.append(Parameter(name='North Shading (m)',
                               selector=GenericSelector(set=north_shading),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    def east_shading(building, value):
        building.add_overhangs(depth = value, tilt = 90, orientation = "east")
        return
    
    parameters.append(Parameter(name='East Shading (m)',
                               selector=GenericSelector(set=east_shading),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    def south_shading(building, value):
        building.add_overhangs(depth = value, tilt = 90, orientation = "south")
        return
    
    parameters.append(Parameter(name='South Shading (m)',
                               selector=GenericSelector(set=south_shading),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    def west_shading(building, value):
        building.add_overhangs(depth = value, tilt = 90, orientation = "west")
        return
    
    parameters.append(Parameter(name='West Shading (m)',
                               selector=GenericSelector(set=west_shading),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)

    #The orientation of the building's project nrth in degrees away from true-north, 0 would be directly north/south facing. The building is a rectangle, with the north/south facades being longer than the east/west. If 90 is entered into the parameter, the 'north' facade now would face due east. Correspondingly, the 'south' facade would now face due west. 
    #Default should be 0, or truth north.
    parameters.append(Parameter(name='Orientation (degrees from north)',
                               selector=FieldSelector(class_name='Building',object_name='*',field_name='North Axis'),
                               value_descriptor=RangeParameter(min_val=-90,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    return parameters, defaults, parameter_metadata_list