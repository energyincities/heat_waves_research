from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy

def renewable_parameters():
    """
    This set of parameters modifies the renewable energy generation and storage. Currently the only parameter is PV generation. Future features to include batteries and grid integration.
    """
    
    group_name="Renewables"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #Modify nominal kW of PV
    
    def set_w_to_wpm2(building, value):
        selector = FieldSelector(class_name='Generator:PVWatts', object_name='Solar PV',field_name='DC System Capacity')
        roofarea=4980/3
        selector.set(building,value*roofarea)     
        return
    
    parameters.append(Parameter(name='PV Capacity (Wp/m2(roof))',
                               selector=GenericSelector(set=set_w_to_wpm2),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=500)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    parameters.append(Parameter(name='PV Tilt (degrees from horizontal)',
                               selector=FieldSelector(class_name='Generator:PVWatts', object_name='Solar PV',field_name='Tilt Angle'),
                               value_descriptor=RangeParameter(min_val=0,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:30})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    parameters.append(Parameter(name='PV Azimuth (degrees from north)',
                               selector=FieldSelector(class_name='Generator:PVWatts', object_name='Solar PV',field_name='Azimuth Angle'),
                               value_descriptor=RangeParameter(min_val=90,max_val=270)))
    defaults.update({parameters[-1].value_descriptors[0].name:180})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    return parameters, defaults, parameter_metadata_list