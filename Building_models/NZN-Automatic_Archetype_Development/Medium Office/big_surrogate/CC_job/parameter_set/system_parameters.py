from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy

def system_parameters():
    """
    This parameter set modifies the sizing of terminal units as a way to select HVAC system types in a continuous manner.
    Systems included:
    VAV, Chilled Beams + DOAS, FCUs + DOAS, HW Baseboards
    
    The method may be switched to a categorical parameter, either 0.001 (not installed) or 1 (installed) and then potentially a different parameter to modify the relative size (oversized, undersized, sensitivity to sizing choice)
    
    This methodology is a unique way to do large parametric sets with multiple mechanical systems in a way that is integrated into one single .idf file. It removes the need to do programatic changes to the idf file within parametrics.
    The caveat is that loads are established and dealt with in a hierarchical order:
    First, the DOAS supplies outdoor air
    Second, the chilled beam supplies ventilation air and meets whatever cooling load it can
    Third, the VAV system meets the loads,
    Fourth, the FCU meets the loads,
    Fifth, the baseboards meet the loads.
    
    Therefore, if a FCU and a VAV unit are both sized at 1, only the FCU will only meet whatever loads the VAV cannot meet. This *could* represent a type of parallel-box system setup where a separate fan kicks on to meet heating loads while the VAV is at minimum flow nad supplying required ventilation loads, for instance. 
    
    For the NZN application, however, this level of detail may be overwhelming to a novice user. 
    
    The general sizing objects may be modified so that each zone uses a sizing object for each respective mechanical system. This would be more programmatively intensive on the parametric side, but would allow for partial-installations of equipment. For example, all core-units could be VAV systems while all perimeter could be served by FCUs, etc. 
    
    
    
    The system selection is being revised to a categorical method which will be transformed into one-hot encoding for ML training.
    """
    
    #parameter list to be updated for multiple field names
    group_name="Air System Selection"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This parameter set modifies 3 fields in the VAV sizing object, for two different sets (the general sizing and the core VAV sizing). 
    #The default should be 0.001 to signify there is effectively no VAV system. 
    vav_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    vav_selectors=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General VAV Sizing',field_name=field) for field in vav_fields]
    vav_selectors.extend([FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='Core VAV Sizing',field_name=field) for field in vav_fields])
    def set_vav(building, value):
        for selector in vav_selectors: selector.set(building,value) 
        return
    
    #This modifies 3 different fields within the chilled beam sizing objects.
    #The default should be 0.001 to signify no chilled beam is installed. 
    cb_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    cb_selectors=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General CB Sizing',field_name=field) for field in cb_fields]
    def set_cb(building, value):
        for selector in cb_selectors: selector.set(building,value) 
        return    

    
    #This modifies 4 different fields within the FCU sizing object
    #A default sizing of 1.0 indicates an FCU is installed at the default autosize. 
    fcu_fields = ['Cooling Fraction of Autosized Cooling Supply Air Flow Rate','Heating Fraction of Heating Supply Air Flow Rate','Fraction of Autosized Cooling Design Capacity',
                 'Fraction of Autosized Heating Design Capacity']
    fcu_selectors=[FieldSelector(class_name='DesignSpecification:ZoneHVAC:Sizing',object_name='General FCU Sizing',field_name=field) for field in fcu_fields]
    fcu_selectors.append(FieldSelector(class_name='Schedule:Constant',object_name='FCU Sizing',field_name='Hourly Value'))
    def set_fcu(building, value):
        for selector in fcu_selectors: selector.set(building,value) 
        return     
    
    def set_system(building,value):
        if value=='VAV':
            set_vav(building,1)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
        else:
            set_vav(building,0.001)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
        if value=='FCU+DOAS':
            set_fcu(building,1)
            FieldSelector(class_name='ZoneHVAC:FourPipeFanCoil',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
            FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General DOAS Sizing',field_name='Fraction of Minimum Outdoor Air Flow').set(building,1)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:NoReheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
        else:
            set_fcu(building,0.001)
            FieldSelector(class_name='ZoneHVAC:FourPipeFanCoil',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
            FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General DOAS Sizing',field_name='Fraction of Minimum Outdoor Air Flow').set(building,.001)
            FieldSelector(class_name='AirTerminal:SingleDuct:VAV:NoReheat',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
        if value=='Chilled Beam':
            set_cb(building,1)
            FieldSelector(class_name='AirTerminal:SingleDuct:ConstantVolume:CooledBeam',object_name='*',field_name='Availability Schedule Name').set(building,'Always On')
        else:
            set_cb(building,0.001)
            FieldSelector(class_name='AirTerminal:SingleDuct:ConstantVolume:CooledBeam',object_name='*',field_name='Availability Schedule Name').set(building,'Always Off')
    
    
    parameters.append(Parameter(name='Air System Selection',
                               selector=GenericSelector(set=set_system),
                               value_descriptors=CategoryParameter(['VAV','FCU+DOAS','Chilled Beam'])))
    defaults.update({parameters[-1].value_descriptors[0].name:'FCU+DOAS'})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"category",
        "Onehot Grouping":"Air System Selection",
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
     
    #This sizes the baseboard terminals.
    # A default size of 0.001 indicates that hot water terminals are not installed.
    # A future potential would be to include electric baseboards.
    parameters.append(Parameter(name='Baseboard Sizing',
                               selector=FieldSelector(class_name='ZoneHVAC:Baseboard:RadiantConvective:Water',
                                                      object_name='*',field_name='Fraction of Autosized Heating Design Capacity'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    return parameters, defaults, parameter_metadata_list


def FCU_parameters():
    """
    This set of parameters modifies factors that relate to the fan-coil unit system type
    Potential parameters to investigate adding:
    - Constant volume fans
    - Different OAT temperature points for reset
    
    The FCU parameters assume the DOAS is installed. The DOAS includes a preheat coil, an ERV, a heating coil, and a cooling coil.
    The DOAS currently assumes the preheat coil will heat the OA to -20C as a form of freeze protection for the ERV.
    The freeze protection setpoint as well as preheat coil setpoint should be added as two variables in the future. 
    The energyplus IOReference gives good defaults for freeze protection. 
    
    The economizer/heat recovery bypass controls need to be verified.
    """

    group_name="Fan Coil Unit + Dedicated Outdoor Air System"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This modifies 4 different fields within the FCU sizing object
    #A default sizing of 1.0 indicates an FCU is installed at the default autosize. 
    fcu_fields = ['Cooling Fraction of Autosized Cooling Supply Air Flow Rate','Heating Fraction of Heating Supply Air Flow Rate','Fraction of Autosized Cooling Design Capacity',
                 'Fraction of Autosized Heating Design Capacity']
    fcu_selectors=[FieldSelector(class_name='DesignSpecification:ZoneHVAC:Sizing',object_name='General FCU Sizing',field_name=field) for field in fcu_fields]
    fcu_selectors.append(FieldSelector(class_name='Schedule:Constant',object_name='FCU Sizing',field_name='Hourly Value'))
    def set_fcu(building, value):
        for selector in fcu_selectors: selector.set(building,selector.get(building)[0]*value) 
        return   
    
    parameters.append(Parameter(name='FCU Sizing Factor',
                               selector=GenericSelector(set=set_fcu),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
        
    # The fan pressure, including both internal and external static pressure, is the dominant variable dictating FCU fan energy
    # Default value of 125 Pa, or a half inch of TSP is reasonable for a FCU. 
    parameters.append(Parameter(name='Terminal Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:SystemModel',
                                                      object_name='*',field_name='Design Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=750)))
    defaults.update({parameters[-1].value_descriptors[0].name:125})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The DOAS supply fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 1000 Pa, or 4 inches of static.
    parameters.append(Parameter(name='DOAS Supply Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS Supply Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))    
    defaults.update({parameters[-1].value_descriptors[0].name:1000})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The DOAS exhaust fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 750 Pa, or 3 inches of static.
    parameters.append(Parameter(name='DOAS Exhaust Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS Return Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:750})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #These sets of fields change the sensible effectiveness of the installed ERV. Here we assume that all sensible effectiveness is equal regardless of flow rate - a liberal assumption
    # A default value of 0.70 could represent a typical heat wheel. 
    erv_sen_fields = ['Sensible Effectiveness at 100 Heating Air Flow','Sensible Effectiveness at 75 Heating Air Flow','Sensible Effectiveness at 100 Cooling Air Flow',
                      'Sensible Effectiveness at 75 Cooling Air Flow']
    erv_sen_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS Heat Recovery',field_name=field) for field in erv_sen_fields]
    def set_erv_sen(building, value):
        for selector in erv_sen_selectors: selector.set(building,value) 
        return      
       
    parameters.append(Parameter(name='ERV Sensible Effectiveness',
                               selector=GenericSelector(set=set_erv_sen),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.7})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #These sets of fields change the latent effectiveness of the installed ERV. Here we assume that all latent effectiveness is equal regardless of flow rate - a liberal assumption
    #This variable should be modified to a fraction of sensible effectiveness. 
    # A default value of 0.50 could represent a typical heat wheel. 
    erv_lat_fields = ['Latent Effectiveness at 100 Heating Air Flow','Latent Effectiveness at 75 Heating Air Flow','Latent Effectiveness at 100 Cooling Air Flow',
                      'Latent Effectiveness at 75 Cooling Air Flow']
    erv_lat_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS Heat Recovery',field_name=field) for field in erv_lat_fields]
    def set_erv_lat(building, value):
        for selector in erv_lat_selectors: selector.set(building,value) 
        return      
       
    parameters.append(Parameter(name='ERV Latent Effectiveness',
                               selector=GenericSelector(set=set_erv_lat),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.5})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This is an outdoor air temperature reset that modifies the setpoint that the ERV & Cooling coil attempt to maintain. The heating coil only maintains 11-12C.
    #OAT at low is 13C (could be modified later)
    #A default value could be 18C - a slightly cool supply temperature. Default revised to 21 after comparing internal cooling loads.
    parameters.append(Parameter(name='DOAS Low SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor Low Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:21})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #OAT at high is 23C (could be modified later)
    #A default value could be 18C - a slightly cool supply temperature - revised to 21C
    parameters.append(Parameter(name='DOAS High SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor High Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:21})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"FCU+DOAS"
    })
    parameter_metadata_list.append(parameter_metadata) 
      
    

    return parameters, defaults, parameter_metadata_list

def VAV_parameters():
    """
    This set of parameters modifies factors that relate to the VAV unit system type. Currently the VAV boxes are reheat boxes with a minimum fraction of air flow when occupied. 
    Parameters to add:
    - Sizing temperatures to size to the actual lowest supply temperature. New high-performance VAV system recommendations use 10C as the design temperature. 
    - Choice between fraction vs. constant flow minimum setpoints in boxes
    - Potential for sizing minimum based on other factors (such as optimal ventilation)
    """

    group_name="Built-up VAV"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This parameter set modifies 3 fields in the VAV sizing object, for two different sets (the general sizing and the core VAV sizing). 
    #The default should be 0.001 to signify there is effectively no VAV system. 
    vav_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    vav_selectors_per=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General VAV Sizing',field_name=field) for field in vav_fields]
    def set_vav_perimeter(building, value):
        for selector in vav_selectors_per: selector.set(building,selector.get(building)[0]*value) 
        return
    
    parameters.append(Parameter(name='VAV Perimeter Sizing Factor',
                               selector=GenericSelector(set=set_vav_perimeter),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This parameter only modifies core zone VAV systems sizing. Oversizing core units is done because they are typically in cooling year-round. A temperature strategy like above will be dominated by the core system, and oversizing the box can allow the system to turn-down even more.
    #A default value of 0.001 would be no core VAV terminal. 
    vav_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    vav_selectors_core=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='Core VAV Sizing',field_name=field) for field in vav_fields]
    def set_vav_core(building, value):
        for selector in vav_selectors_core: selector.set(building,selector.get(building)[0]*value) 
        return
    
    parameters.append(Parameter(name='VAV Core Sizing Factor',
                               selector=GenericSelector(set=set_vav_core),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #The minimum box position of the VAV when cooling is not required. Recent ASHRAE and DoE recent highlights that historic rules-of-thumb for minimum requirements tend to be oversized due to fears of improper air mixing, which can have a huge effect on energy consumption for reheat. 
    # default of 0.30 is reasonable. 
    parameters.append(Parameter(name='Minimum Box Position',
                               selector=FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',
                                                      object_name='*',field_name='Constant Minimum Air Flow Fraction'),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.3})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The VAV boxes are set as reheat boxes, where airflow is allowed to increase in heating if coil is at maximum position.
    # Default value of 0.30 means it cannot go beyond minimum flow. 
    parameters.append(Parameter(name='Maximum Reheat Position',
                               selector=FieldSelector(class_name='AirTerminal:SingleDuct:VAV:Reheat',
                                                      object_name='*',field_name='Maximum Flow Fraction During Reheat'),
                               value_descriptor=RangeParameter(min_val=0,max_val=1)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.3})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This assumes a 'warmest' reset, where the temperature meets the cooling demands of the warmest zone (at max flow)
    # A default value of 13 for standard VAVs. 
    parameters.append(Parameter(name='VAV SAT Reset Low (C)',
                               selector=FieldSelector(class_name='SetpointManager:Warmest',
                                                      object_name='Central VAV Cooling Supply Air Temp Manager',field_name='Minimum Setpoint Temperature'),
                               value_descriptor=RangeParameter(min_val=10,max_val=13)))
    defaults.update({parameters[-1].value_descriptors[0].name:13})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #This assumes a 'warmest' reset, where the temperature meets the cooling demands of the warmest zone (at max flow)
    # A maximum is entered of 21C to prevent the VAV system from breaking at temperatures that are too warm.
    
    def vav_reset_high(building,value):
        min_temp=FieldSelector(class_name='SetpointManager:Warmest',
                                                      object_name='Central VAV Cooling Supply Air Temp Manager',field_name='Minimum Setpoint Temperature').get(building)
        max_temp=min(min_temp[0]+value,21)
        FieldSelector(class_name='SetpointManager:Warmest',
                                                      object_name='Central VAV Cooling Supply Air Temp Manager',field_name='Maximum Setpoint Temperature').set(building,max_temp)
    
    parameters.append(Parameter(name='VAV SAT Reset High (C)',
                               selector=GenericSelector(set=vav_reset_high),
                               value_descriptor=RangeParameter(min_val=0,max_val=7)))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
      
    
    #Modify pressure of central fan as way of controlling supply fan power
    parameters.append(Parameter(name='VAV Supply Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='Central VAV Supply Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=10,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:1000})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Modify pressure of central fan as way of controlling return fan power
    parameters.append(Parameter(name='VAV Return Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='Central VAV Return Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=10,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:250})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"VAV"
    })
    parameter_metadata_list.append(parameter_metadata)
    

    

    return parameters, defaults, parameter_metadata_list

def CB_parameters():
    """
    This set of parameters modifies factors that relate to the chilled beam unit system type
    Parameters to add:
    - DOAS temperature control
    - DOAS dessicant
    - DOAS variable air-side flow rate
    - Convert DOAS to mixed-air system to induce zone-air and/or humidity control without additional OA
    - Ventilation rate based on a per-floor area flow rate. Active chilled beam sizing parameters are a balance between induction flow rate and the number of chilled beams installed, effectively balancing capital cost and energy consumption. 
    """

    group_name="Chilled Beam"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #This modifies 3 different fields within the chilled beam sizing objects.
    #The default should be 0.001 to signify no chilled beam is installed. 
    cb_fields = ['Fraction of Design Cooling Load','Fraction of Design Heating Load','Fraction of Minimum Outdoor Air Flow']
    cb_selectors=[FieldSelector(class_name='DesignSpecification:AirTerminal:Sizing',object_name='General CB Sizing',field_name=field) for field in cb_fields]
    def set_cb(building, value):
        for selector in cb_selectors: selector.set(building,selector.get(building)[0]*value) 
        return  
    
    parameters.append(Parameter(name='Chilled Beam Sizing Factor',
                               selector=GenericSelector(set=set_cb),
                               value_descriptor=RangeParameter(min_val=0.7,max_val=1.30)))
    defaults.update({parameters[-1].value_descriptors[0].name:1})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    

    # The DOAS supply fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 1000 Pa, or 4 inches of static.
    parameters.append(Parameter(name='DOAS CB Supply Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS CB Supply Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))    
    defaults.update({parameters[-1].value_descriptors[0].name:1000})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    # The DOAS exhaust fan pressure, including internal and external static pressure, is dominant variable dictating DOAS fan energy
    # Default value of 750 Pa, or 3 inches of static.
    parameters.append(Parameter(name='DOAS CB Exhaust Fan Pressure (Pa)',
                               selector=FieldSelector(class_name='Fan:VariableVolume',
                                                      object_name='DOAS CB Return Fan',field_name='Pressure Rise'),
                               value_descriptor=RangeParameter(min_val=0.001,max_val=2000)))
    defaults.update({parameters[-1].value_descriptors[0].name:750})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)

    #CB ERV TBD
#     erv_sen_fields = ['Sensible Effectiveness at 100% Heating Air Flow','Sensible Effectiveness at 75% Heating Air Flow','Sensible Effectiveness at 100% Cooling Air Flow',
#                       'Sensible Effectiveness at 75% Cooling Air Flow']
#     erv_sen_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS CB Heat Recovery',field_name=field) for field in erv_sen_fields]
#     def set_erv_sen(building, value):
#         for selector in erv_sen_selectors: selector.set(building,value) 
#         return      

#     parameters.append(Parameter(name='ERV Sensible Effectiveness',
#                                selector=GenericSelector(set=set_erv_sen),
#                                value_descriptor=RangeParameter(min_val=0,max_val=1)))

#     erv_lat_fields = ['Latent Effectiveness at 100% Heating Air Flow','Latent Effectiveness at 75% Heating Air Flow','Latent Effectiveness at 100% Cooling Air Flow',
#                       'Latent Effectiveness at 75% Cooling Air Flow']
#     erv_lat_selectors=[FieldSelector(class_name='HeatExchanger:AirToAir:SensibleAndLatent',object_name='DOAS CB Heat Recovery',field_name=field) for field in erv_lat_fields]
#     def set_erv_lat(building, value):
#         for selector in erv_lat_selectors: selector.set(building,value) 
#         return      

#     parameters.append(Parameter(name='ERV Latent Effectiveness',
#                                selector=GenericSelector(set=set_erv_lat),
#                                value_descriptor=RangeParameter(min_val=0,max_val=1)))

    #This is an outdoor air temperature reset that modifies the setpoint that the ERV & Cooling coil attempt to maintain. The heating coil only maintains 11-12C.
    #OAT at low is 13C (could be modified later)
    # Default of 13C for humidity control for now
    parameters.append(Parameter(name='DOAS CB Low SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS CB Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor Low Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:13})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #OAT at high is 23C (could be modified later)
    # Default of 13C for humitiy control for now
    parameters.append(Parameter(name='DOAS CB High SAT (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='DOAS CB Cooling Supply Air Temp Manager',field_name='Setpoint at Outdoor High Temperature'),
                               value_descriptor=RangeParameter(min_val=13,max_val=26)))
    defaults.update({parameters[-1].value_descriptors[0].name:13})
    parameter_metadata=({
        "label":parameters[-1].value_descriptors[0].name,
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"Air System Selection",
        "Dependent Parameter Value":"Chilled Beam"
    })
    parameter_metadata_list.append(parameter_metadata)
    

    return parameters, defaults, parameter_metadata_list