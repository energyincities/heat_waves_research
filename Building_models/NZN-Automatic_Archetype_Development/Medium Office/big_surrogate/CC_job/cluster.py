import argparse
import numpy as np
import pandas as pd
import time
import pickle
import sys
from besos.evaluator import EvaluatorEP
from besos import eppy_funcs as ef

nzn_pathway='./parameter_set'
sys.path.append(nzn_pathway)

from architectural_parameters import architectural_parameters
from comfort_parameters import comfort_parameters
from envelope_parameters import envelope_parameters
from lighting_parameters import lighting_parameters
from loads_parameters import loads_parameters
from system_parameters import system_parameters
from system_parameters import FCU_parameters
from system_parameters import VAV_parameters
from system_parameters import CB_parameters
from plant_parameters import heating_plant_parameters
from plant_parameters import cooling_plant_parameters
from renewable_parameters import renewable_parameters

import NZN_Office_output_set
from NZN_Office_output_set import fuel_use
from NZN_Office_output_set import end_use_breakdown
from NZN_Office_output_set import metrics

import besos.objectives as objectives
from besos.problem import EPProblem


now = time.time() # get the starting timestamp

#################################################################################################
# ##########################   Set up argparser here  ############################################
# ################################################################################################

parser = argparse.ArgumentParser(description='Process the Job IDs to retrieve the inputs.')
parser.add_argument('task_id', metavar='N', type=int, nargs='+',
                   help='Task id to pick inputs')
parser.add_argument('samples_per_task', metavar='N', type=int, nargs='+',
                   help='Number of samples to run per task.')
args = parser.parse_args()

#################################################################################################
# ########################## Define the Building here ############################################
# ################################################################################################

epw_victoria='CAN_BC_VictoriaUniversityCS_offset_from_Victoria-Univ.of.Victoria.717830_CWEC2016.epw'

building = ef.get_building('NZN_Medium_Office_Default.idf', data_dict='Energy+.idd', ep_path='~')



parameter_sets=[architectural_parameters,comfort_parameters,envelope_parameters,lighting_parameters,loads_parameters,system_parameters,FCU_parameters,VAV_parameters,
               CB_parameters,heating_plant_parameters,cooling_plant_parameters,renewable_parameters]

parameters=[]
for i in parameter_sets:
    parameters.extend(i()[0])

outputs = fuel_use()
outputs.extend(end_use_breakdown())
outputs.extend(metrics())
#Drop any meters that don't currently exist in E+
_=[outputs.remove(s) for s in ['InteriorEquipment:Gas','ExteriorEquipment:Electricity','Humidifier:Gas']]

    
problem = EPProblem(parameters, outputs)
evaluator = EvaluatorEP(problem, building, epw = epw_victoria,error_mode='Silent', error_value=None, ep_path='~')

#################################################################################################
# ##########################  Load samples & run job  ############################################
# ################################################################################################    

print(args.task_id)
inputs = pd.read_csv('inputs_10000.csv', index_col='Unnamed: 0').iloc[(args.task_id[0]-1)*args.samples_per_task[0]:args.task_id[0]*args.samples_per_task[0],:]    
print(f'Loaded {len(inputs)} inputs.')
outputs = evaluator.df_apply(inputs, keep_input=True)
print('Ran the samples.')
#################################################################################################
# ##########################    Store samples here    ############################################
# ################################################################################################    

outputs.to_csv(f'outputs_{args.task_id[0]}.csv')

passedtime = round(time.time()-now,2)
timestr = 'Time to evaluate '+str(len(inputs))+' samples: '+str(passedtime)+' seconds.'

with open('time.cluster', 'wb') as timecluster:
     pickle.dump(timestr, timecluster)
with open('op.out', 'wb') as op:
     pickle.dump(outputs, op)
        
print(timestr)
