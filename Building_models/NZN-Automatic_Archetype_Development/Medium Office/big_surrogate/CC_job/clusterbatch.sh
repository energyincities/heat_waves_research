#!/bin/bash
#SBATCH --account=def-revins

#SBATCH --array=1-250
#SBATCH --time=02:15:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=2000mb
#SBATCH --mail-user=pwestermann@uvic.ca
#SBATCH --mail-type=ALL


#!generate the virtual environment
module load python/3.6

source ~/env/bin/activate

echo "prog started at: `date`"

srun python cluster.py $SLURM_ARRAY_TASK_ID 40

deactivate
echo "prog ended at: `date`"
