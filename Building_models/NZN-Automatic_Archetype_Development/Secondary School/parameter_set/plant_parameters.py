from besos.parameters import wwr, RangeParameter, FieldSelector, FilterSelector, CategoryParameter, GenericSelector, Parameter, expand_plist, Descriptor
import numpy as numpy



def heating_plant_parameters():
    """
    This set of parameters modifies the selection and design of the heating equipment
    
    In order to demand flow through some plant equipment, the plant has been set up as primary/secondary with a common pipe.
    The primary flow is constant volume. The secondary flow is variable volume.
    There is a full-sized boiler and chiller at bottom priority on each primary loop to request flow whenever there is a load, otherwise the ASHP (etc) may not recognize and demand flow.
    The plant loop equipment is set up in a hierarchical manner
    
    For heating:
    First, the ASHP meets whatever load it can
    Second, the first hot water NG boiler,
    Third, the second hot water NG boiler,
    Fourth, the load is met by a district heating systems
    Fifth, an electric boiler is there to demand flow, but currently will not see any load after the district
    """
    group_name="Heating Plant Design"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    #Boiler-1 and boiler-2 are both condensing natural gas boilers. Nominal efficiency is measured at 82C temp (non-condensing range)
    #The relative size of boiler-1(NG). This should be kept as a continuous variable.
    # A reasonable default would be 0.5, or sized at 50% of the peak load.
    
    def gas_boiler_quantity(building,value):

        if value==0:
            boiler_1=0.00001
            boiler_2=0.00001
        if value==1:
            boiler_1=1
            boiler_2=0.00001
        else:
            boiler_1=1
            boiler_2=1

        FieldSelector(class_name='Boiler:HotWater',object_name='Boiler-1',field_name='Maximum Part Load Ratio').set(building,boiler_1)
        FieldSelector(class_name='Boiler:HotWater',object_name='Boiler-2',field_name='Maximum Part Load Ratio').set(building,boiler_2)
    
    
    parameters.append(Parameter(name='Gas Boiler Quantity',
                               selector=GenericSelector(set=gas_boiler_quantity),
                               value_descriptor=CategoryParameter(['None','One','Two'])))
    defaults.update({parameters[-1].value_descriptors[0].name:'Two'})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"category",
        "Categories": ['None','One','Two'],
        "Onehot Grouping":'Gas Boiler Quantity',
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)    
    
    
    
    
    #Boiler-1 and boiler-2 are both condensing natural gas boilers. Nominal efficiency is measured at 82C temp (non-condensing range)
    #The relative size of boiler-1(NG). This should be kept as a continuous variable.
    # A reasonable default would be 0.5, or sized at 50% of the peak load.
    parameters.append(Parameter(name='Boiler-1 Sizing',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-1',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.2,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.5})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),        
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":['One','Two'],        
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #The relative size of boiler-2(NG). This should be kept as a continuous variable.
    # A reasonable default would be 0.5, or sized at 50% of the peak load.
    parameters.append(Parameter(name='Boiler-2 Sizing',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-2',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.2,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.5})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":['Two'],      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
        #Nominal boiler efficiency. Nominal efficiency is at full-load conditions and at a SWT of 82C. It assumes that all boilers are condensing, but that condensing only occurs at the proper temperatures. A high-efficiency condensing boiler will have a nominal efficiency of 86% at these conditions. It may be possible at very low conditions that efficiency may be above 100% for short periods of low-load.
    
    #Default thermal efficiency of 0.82 (or 82%) would results in a peak efficiency of 96% or so.
    parameters.append(Parameter(name='Boiler-1 Nominal Efficiency (%)',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-1',field_name='Nominal Thermal Efficiency'),
                               value_descriptor=RangeParameter(min_val=.5,max_val=.88)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.82})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":['One','Two'],    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Default thermal efficiency of 0.82 would results in a peak efficiency of 96% or so.
    parameters.append(Parameter(name='Boiler-2 Nominal Efficiency (%)',
                               selector=FieldSelector(class_name='Boiler:HotWater',
                                                      object_name='Boiler-2',field_name='Nominal Thermal Efficiency'),
                               value_descriptor=RangeParameter(min_val=.5,max_val=.88)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.82})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":'Gas Boiler Quantity',
        "Dependent Parameter Value":['Two'],    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #ASHP is separated into heating and cooling. They are not connected.
    #Default value of 0.001 would be no ASHP for heating
    #Adding a conditional, where if the sizing is very small the ASHPs are not activated.
    
    
    def ashp_heater_enable(building,value):
       
        if value<.9:
            eqptlist_heater='All Heaters-2'
        else:
            eqptlist_heater='All Heaters'
           
        FieldSelector(class_name='PlantEquipmentOperation:HeatingLoad',object_name='HWL-1 Operation All Hours',field_name='Range 1 Equipment List Name').set(building,eqptlist_heater)
        return
        
    
    parameters.append(Parameter(name='ASHP Heating Enable',
                               selector=GenericSelector(set=ashp_heater_enable),
                               value_descriptor=CategoryParameter([0,1])))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Air Source Heat Pump Enable",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"boolean",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,        
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    parameters.append(Parameter(name='ASHP Heating Sizing',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Heating',object_name='ASHP Heating',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.001,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Air Source Heat Pump Heating Sizing",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Heating Enable",
        "Dependent Parameter Value":[1],     
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
        
    #ASHP Heating efficiency is determined at an OAT of 7C and a SWT of 45C at full-load. It is modelled after an AERMEC NRL 280-750 (HA-HE version) with nominal heating COP of approximately 3
    #Default value of 3 is reasonable. 
    parameters.append(Parameter(name='ASHP Heating Efficiency (COP)',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Heating',
                                                      object_name='ASHP Heating',field_name='Reference Coefficient of Performance'),
                               value_descriptor=RangeParameter(min_val=2,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:3})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Air Source Heat Pump Heating Efficiency (Coefficient of Performance)",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Heating Enable",
        "Dependent Parameter Value":[1],      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
                      
      #These factors are used to size the flow rate in the plant loops
    #Default of 20C would be reasonable for a high-performance hot water loop
    parameters.append(Parameter(name='Hot Water DeltaT (C)',
                               selector=FieldSelector(class_name='Sizing:Plant',
                                                      object_name='HWL-1 Hot Water Loop',field_name='Loop Design Temperature Difference'),
                               value_descriptor=RangeParameter(min_val=5,max_val=25)))
    defaults.update({parameters[-1].value_descriptors[0].name:20})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Hot Water Change in Temperature",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,         
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
                      
    #These parameters will alter the supply water temperature for the heating, loops as well as the design temperature
    
    #Hot Water Loop
    #Low OAT is at -25 for the HWL and -10 for the ASHP outlet, which is an equipment limit
    #Default of 60C for a low-temperature HWL
    HW_low_selectors=[]
    HW_low_selectors.append(FieldSelector(class_name='Sizing:Plant',
                                                      object_name='HWL-1 Hot Water Loop',field_name='Design Loop Exit Temperature'))    
    HW_low_selectors.append(FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='HWL-1 HW Temp Manager',field_name='Setpoint at Outdoor Low Temperature'))
    
    def set_HW_low(building, value):
        for selector in HW_low_selectors: selector.set(building,value) 
        return  
    
    parameters.append(Parameter(name='Hot Water Low Temp (C)',
                               selector=GenericSelector(set=set_HW_low),
                               value_descriptor=RangeParameter(min_val=45,max_val=82)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Reset temperature at warm temperature, currently set at 15C OAT
    #Default of 40C for a low-temperature HWL 
    parameters.append(Parameter(name='Hot Water High Temp (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='HWL-1 HW Temp Manager',field_name='Setpoint at Outdoor High Temperature'),
                               value_descriptor=RangeParameter(min_val=30,max_val=82)))
    defaults.update({parameters[-1].value_descriptors[0].name:40})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None, 
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)    
                      



    #Hot water supply pump head. Min - 20 ft, max - 90 ft
    #Assume default of 60 ft
    def set_HW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='HWL-1 HW Demand Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3) 
        return  
    parameters.append(Parameter(name='Hot Water Head (ft)',
                               selector=GenericSelector(set=set_HW_pump),
                               value_descriptor=RangeParameter(min_val=20,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None, 
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)    
    
    #Default will be 0.001 to ignore the pump.
    def set_prim_HW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='HWL-1 HW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)     
        return  
    parameters.append(Parameter(name='Primary Hot Water Head (ft)',
                               selector=GenericSelector(set=set_prim_HW_pump),
                               value_descriptor=RangeParameter(min_val=.001,max_val=60)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,   
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Plant - heating parameters start at 600
    
    def Display_Order(parameter_metadata_list):
        x = 600
        for metadata_dict in parameter_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(parameter_metadata_list)
    
    return parameters, defaults, parameter_metadata_list

    


def cooling_plant_parameters():
    """
    This set of parameters modifies the selection of plant equipment
    
    In order to demand flow through some plant equipment, the plant has been set up as primary/secondary with a common pipe.
    The primary flow is constant volume. The secondary flow is variable volume.
    There is a full-sized boiler and chiller at bottom priority on each primary loop to request flow whenever there is a load, otherwise the ASHP (etc) may not recognize and demand flow.
    The plant loop equipment is set up in a hierarchical manner
        
    For cooling:
    First, the water-side cooling meets whatever load it can.
    Second, the ASHP meets whatever load it can
    Third, the first water-cooled chiller,
    Fourth, the second water-cooled chiller,
    Fifth, the first air-cooled chiller,
    Sixth, the second air-cooled chiller,
    Seventh, the load is met by a district cooling systems
    Eighth, an air-cooled chiller is there to demand flow, but currently will not see any load after the district
    
    This ordering will be updated with further equipment.
    There is also a combination of series vs. parallel equipment.
    
    The ASHP currently meets as much load as it can before branching into the other equipment depending on where on the load list it is. Therefore, the temperature entering the branched equipment will be slightly-conditioned.
    As more equipment is added it will either be set in series w/ the ASHP or in parallel with the main equipment.
    
    """
    
    group_name="Cooling Plant Design"
    
    parameters=[]
    
    defaults={}
    
    parameter_metadata_list=[]
    
    
    chiller_type=CategoryParameter(['Air-Cooled','Water-Cooled','Mixed'],name='Chiller Type')
    chiller_number=CategoryParameter(['None','One','Two'],name='Chiller Quantity')
    chiller_1_sizing=RangeParameter(min_val=0.1,max_val=1.3,name='Chiller-1 Size')
    chiller_2_sizing=RangeParameter(min_val=0.1,max_val=1.3,name='Chiller-2 Size')
    chiller_1_eff=RangeParameter(min_val=1,max_val=8,name='Chiller-1 Efficiency')
    chiller_2_eff=RangeParameter(min_val=1,max_val=8,name='Chiller-2 Efficiency')

    def chiller_design(building,chiller_type,chiller_number,chiller_1_sizing,chiller_2_sizing,chiller_1_eff,chiller_2_eff):
        chiller_1_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-1',field_name='Sizing Factor')
        chiller_1_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-1',field_name='Reference COP')
        chiller_2_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-2',field_name='Sizing Factor')
        chiller_2_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-2',field_name='Reference COP')
        chiller_3_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-3',field_name='Sizing Factor')
        chiller_3_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-3',field_name='Reference COP')
        chiller_4_sel=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-4',field_name='Sizing Factor')
        chiller_4_sel_eff=FieldSelector(class_name='Chiller:Electric:EIR',object_name='Chill-4',field_name='Reference COP')

        chiller_1_sel.set(building,0.0001)
        chiller_2_sel.set(building,0.0001)
        chiller_3_sel.set(building,0.0001)
        chiller_4_sel.set(building,0.0001)

        if chiller_type=='Air-Cooled':
            chiller_sels=[chiller_3_sel,chiller_4_sel]
            chiller_effs=[chiller_3_sel_eff,chiller_4_sel_eff]
        if chiller_type=='Water-Cooled':
            chiller_sels=[chiller_1_sel,chiller_2_sel]
            chiller_effs=[chiller_1_sel_eff,chiller_2_sel_eff]        
        if chiller_type=='Mixed':
            chiller_sels=[chiller_3_sel,chiller_1_sel]
            chiller_effs=[chiller_3_sel_eff,chiller_1_sel_eff]        

        if chiller_number=='None':
            return
        if chiller_number=='One':
            chiller_sels[0].set(building,chiller_1_sizing)
            chiller_effs[0].set(building,chiller_1_eff)
        if chiller_number=='Two':
            chiller_sels[0].set(building,chiller_1_sizing)
            chiller_sels[1].set(building,chiller_2_sizing)
            chiller_effs[0].set(building,chiller_1_eff)
            chiller_effs[1].set(building,chiller_2_eff)

        return

    parameters.append(Parameter(
        selector=GenericSelector(set = chiller_design),
        # the values from these descriptors are passed to the function in order
        value_descriptors=[chiller_type,chiller_number,chiller_1_sizing,chiller_2_sizing,chiller_1_eff,chiller_2_eff]
        ))
    description_list=["**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)",
                     "**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)",
                     "**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)",
                     "**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)",
                     "**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)",
                     "**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)",
                     "**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"]
    defaults_list=['Air-Cooled','Two',.5,.5,3,3]
    onehot_list=['Chiller Type','Chiller Quantity',None,None,None,None]
    category_list=[['Air-Cooled','Water-Cooled','Mixed'],['None','One','Two'], None, None, None, None]
    dep_par_list=[None,None,'Chiller Quantity','Chiller Quantity','Chiller Quantity','Chiller Quantity']
    dep_val_list=[None,None,['One','Two'],['Two'],['One','Two'],['Two']]
    for i in range(len(parameters[-1].value_descriptors)):
        defaults.update({parameters[-1].value_descriptors[i].name:defaults_list[i]})
        parameter_metadata=({
            "Label":parameters[-1].value_descriptors[i].name,
            "Long Name": None,         
            "Description":description_list[i],
            "Min":parameters[-1].value_descriptors[i].min if isinstance(parameters[-1].value_descriptors[i],RangeParameter) else None,
            "Max":parameters[-1].value_descriptors[i].max if isinstance(parameters[-1].value_descriptors[i],RangeParameter) else None,
            "Default":defaults.get(parameters[-1].value_descriptors[i].name),
            "Parameter Group":group_name,
            "Type":"continuous" if isinstance(parameters[-1].value_descriptors[i],RangeParameter) else "boolean" if parameters[-1].value_descriptors[1].options==[0,1] else "category",
            "Categories": category_list[i],
            "Onehot Grouping":onehot_list[i],
            "Dependent Parameter":dep_par_list[i],
            "Dependent Parameter Value":dep_val_list[i],        
            "Displayed Order": None,         
            "Visibility": "Hidden",         
            "Pinned": None
        })
        parameter_metadata_list.append(parameter_metadata)    
    
    
    
    
    
    
    #Default value of 0.001 would be no ASHP for cooling
    
    def ashp_chiller_enable(building,value):
        if value<0.9:
            eqptlist_chiller='All Chillers-2'
        else:
            eqptlist_chiller='All Chillers'
           
        FieldSelector(class_name='PlantEquipmentOperation:CoolingLoad',object_name='ChW-1 Chiller Operation All Hours',field_name='Range 1 Equipment List Name').set(building,eqptlist_chiller)
        return
    
    
    parameters.append(Parameter(name='ASHP Cooling Enable',
                               selector=GenericSelector(set=ashp_chiller_enable),
                               value_descriptor=CategoryParameter([0,1])))
    defaults.update({parameters[-1].value_descriptors[0].name:0})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Air Source Heat Pump Cooling Enable",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":None,
        "Max":None,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"boolean",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,     
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    parameters.append(Parameter(name='ASHP Cooling Sizing',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Cooling',object_name='ASHP Cooling',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.001,max_val=1.3)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Air Source Heap Pump Cooling Sizing",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Cooling Enable",
        "Dependent Parameter Value":1,       
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
        #ASHP Cooling efficiency is determined at an OAT of 35C and a SWT of 6.7C at full-load. It is modelled after an AERMEC NRL 280-750 (HA-HE version) with nominal cooling COP of approximately 3
    #Default value of 3 is reasonable. 
    parameters.append(Parameter(name='ASHP Cooling Efficiency (COP)',
                               selector=FieldSelector(class_name='HeatPump:PlantLoop:EIR:Cooling',
                                                      object_name='ASHP Cooling',field_name='Reference Coefficient of Performance'),
                               value_descriptor=RangeParameter(min_val=2,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:3})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Air Source Heat Pump Cooling Effifiency (Coefficient of Performance)",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":"ASHP Cooling Enable",
        "Dependent Parameter Value":1,      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    
    #Water-side economizer is a heat exchanger between a dry-cooler enabled condenser loop at the cooling loop
    #Default value of 0.001 would be no water-side economizer
    parameters.append(Parameter(name='Waterside Economizer Sizing',
                               selector=FieldSelector(class_name='HeatExchanger:FluidToFluid',
                                                      object_name='Waterside Economizer',field_name='Sizing Factor'),
                               value_descriptor=RangeParameter(min_val=.001,max_val=1.5)))
    defaults.update({parameters[-1].value_descriptors[0].name:0.001})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    

    #Default of 6C would be reasonable. 
    parameters.append(Parameter(name='Chilled Water DeltaT (C)',
                               selector=FieldSelector(class_name='Sizing:Plant',
                                                      object_name='ChW-1 Chilled Water Loop',field_name='Loop Design Temperature Difference'),
                               value_descriptor=RangeParameter(min_val=3,max_val=10)))
    defaults.update({parameters[-1].value_descriptors[0].name:6})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Chiller Water Change in Temperature",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None, 
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Default of 4 is reasonable. 
    parameters.append(Parameter(name='Condenser Water DeltaT (C)',
                               selector=FieldSelector(class_name='Sizing:Plant',
                                                      object_name='ChW-1 Condenser Water Loop',field_name='Loop Design Temperature Difference'),
                               value_descriptor=RangeParameter(min_val=3,max_val=10)))
    defaults.update({parameters[-1].value_descriptors[0].name:4})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Condenser Water Change in Temperature",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    

    
    #Chilled Water Loop
    #High OAT is at 20C. This may need to be modified if humidity control requires lower temperature
    #Defualt of 7 for a typical chilled water loop. 
    ChW_high_selectors=[]
    ChW_high_selectors.append(FieldSelector(class_name='Sizing:Plant',
                                                      object_name='ChW-1 Chilled Water Loop',field_name='Design Loop Exit Temperature'))    
    ChW_high_selectors.append(FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='ChW-1 Temp Manager',field_name='Setpoint at Outdoor High Temperature'))
    
    def set_ChW_high(building, value):
        for selector in ChW_high_selectors: selector.set(building,value) 
        return  
    
    parameters.append(Parameter(name='Chilled Water High Temp (C)',
                               selector=GenericSelector(set=set_ChW_high),
                               value_descriptor=RangeParameter(min_val=4,max_val=8)))
    defaults.update({parameters[-1].value_descriptors[0].name:7})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Reset temperature at warm temperature, currently set at 12C
    # Default of 7C, or no reset. 
    parameters.append(Parameter(name='Chilled Water Low Temp (C)',
                               selector=FieldSelector(class_name='SetpointManager:OutdoorAirReset',
                                                      object_name='ChW-1 Temp Manager',field_name='Setpoint at Outdoor Low Temperature'),
                               value_descriptor=RangeParameter(min_val=4,max_val=12)))
    defaults.update({parameters[-1].value_descriptors[0].name:7})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,        
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #Condenser Water Loop
    #The condenser water loop is controlled via EMS to follow the outside air wetbulb temperature with a deadband and limited by a minimum lift limit. The lift is measured as the difference between chilled water supply setpoint and condenser water return setpoint
    #A default minimum lift would be around 4C, as a reasonable number. 
    parameters.append(Parameter(name='Condenser Minimum Lift (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='Lift Min',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=0,max_val=20)))
    defaults.update({parameters[-1].value_descriptors[0].name:4})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,        
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #A reasonable offset would be to 2C. 
    parameters.append(Parameter(name='Condenser Setpoint WB Offset (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='CT WB Offset',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=0,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:2})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": "Condenser Setpoint Wet-Bulb Temperature Offset",         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #The condenser loop can be disabled at different outdoor air temperatures to disable winter operation.This may break the data center cooling though.
    # Disable to cooling towers at -40 for default.  
    parameters.append(Parameter(name='Cooling Tower Loop Disable Temperature (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='CT Disable',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=-40,max_val=5)))
    defaults.update({parameters[-1].value_descriptors[0].name:-40})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    #The free cooling loop can be enabled at different outdoor air temperatures to enable winter operation
    # The default would be no free cooling installed, so we can set this default higher to something like 4C. 
    parameters.append(Parameter(name='Free Cooling Loop Enabled Temperature (C)',
                               selector=FieldSelector(class_name='Schedule:Constant',
                                                      object_name='Free Cool Enable',field_name='Hourly Value'),
                               value_descriptor=RangeParameter(min_val=-20,max_val=15)))
    defaults.update({parameters[-1].value_descriptors[0].name:4})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,      
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    

    

    
    
    
    
    #Chilled water supply pump head. Min - 20 ft, max - 90 ft. It assumes that both the beam loop and secondary chilled water loop are equivalent.
    #Assume default of 60 ft
    def set_ChW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='ChW-1 ChW Demand Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='Beam ChW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)        
        return  
    parameters.append(Parameter(name='Chilled Water Head (ft)',
                               selector=GenericSelector(set=set_ChW_pump),
                               value_descriptor=RangeParameter(min_val=20,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,    
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    

    
    #Condenser water supply pump head. Min - 20 ft, max - 90 ft
    #Assume default of 60 ft
    def set_CndW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='ChW-1 CndW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3) 
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='Free Cooling CndW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3) 
        return  
    parameters.append(Parameter(name='Condenser Water Head (ft)',
                               selector=GenericSelector(set=set_CndW_pump),
                               value_descriptor=RangeParameter(min_val=20,max_val=90)))
    defaults.update({parameters[-1].value_descriptors[0].name:60})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None,         
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    
    #Primary pump parameters, which are set to always constant. 
    #Default will be 0.001 to ignore the pump.
    def set_prim_ChW_pump(building, value):
        selector = FieldSelector(class_name='Pump:VariableSpeed', object_name='ChW-1 ChW Supply Pump',field_name='Design Pump Head')
        selector.set(building,value*2899.3)     
        return  
    parameters.append(Parameter(name='Primary Chilled Water Head (ft)',
                               selector=GenericSelector(set=set_prim_ChW_pump),
                               value_descriptor=RangeParameter(min_val=.001,max_val=60)))
    defaults.update({parameters[-1].value_descriptors[0].name:.001})
    parameter_metadata=({
        "Label":parameters[-1].value_descriptors[0].name,
        "Long Name": None,         
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"), 
        "Min":parameters[-1].value_descriptors[0].min,
        "Max":parameters[-1].value_descriptors[0].max,
        "Default":defaults.get(parameters[-1].value_descriptors[0].name),
        "Parameter Group":group_name,
        "Type":"continuous",
        "Onehot Grouping":None,
        "Dependent Parameter":None,
        "Dependent Parameter Value":None, 
        "Displayed Order": None,         
        "Visibility": "Hidden",         
        "Pinned": None
    })
    parameter_metadata_list.append(parameter_metadata)
    
    ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Plant - cooling parameters start at 700
    
    def Display_Order(parameter_metadata_list):
        x = 700
        for metadata_dict in parameter_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(parameter_metadata_list)
    
    return parameters, defaults, parameter_metadata_list
