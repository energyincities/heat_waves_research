"""
This python file will document some combinations of meter outputs from an idf file.

To add:
- detailed breakdown (by equipment type)
- additional details like sizing
"""

def fuel_use():
    
    """
    Outputs that display the energy usage of the building, broken down by fueltype.
    """
    
    group_name="Results - Fuel Use"
    
    output_metadata_list=[]
    
    outputs=[]

# # Fuel Use Results include the following:    

    outputs.append('Electricity:Facility')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Consumption - Electricity (ekWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Shown",
        "Pinned": None,
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Gas:Facility')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Consumption - Gas (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Shown",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('DistrictHeating:Facility')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Consumption - District Heating (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Shown",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('DistrictCooling:Facility')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Consumption - District Cooling (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Shown",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Output - Fuel Use start at 2000
    
    def Display_Order(output_metadata_list):
        x = 2000
        for metadata_dict in output_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(output_metadata_list)
    
    return outputs, output_metadata_list

def metrics():
    
    
    """
    Metrics that measure the intensity of total energy usage and thermal energy usage in the building.
    """
    
    group_name="Results - Metrics"
    
    output_metadata_list=[]
    
    outputs=[]

# # Metrics Results include the following:  

    outputs.append('TED')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": "Thermal Energy Demand Intensity",
        "Displayed Name": "TEDI (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Shown",
        "Pinned": True,
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('TEU')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": "Total Energy Use Intensity",
        "Displayed Name": "TEUI (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Shown",
        "Pinned": True
    })
    output_metadata_list.append(output_metadata)
    
    ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Output - Metrics start at 2200
    
    def Display_Order(output_metadata_list):
        x = 2200
        for metadata_dict in output_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(output_metadata_list)
    
    return outputs, output_metadata_list

def end_use_breakdown():
    
    """
    Outputs that display the breakdown of how energy was used in the building, further broken down by fueltype.
    """
    
    group_name="Results - End Use Breakdown"
    
    output_metadata_list=[]
    
    outputs=[]

# # End Use Breakdown Results include the following:

    outputs.append('Heating:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Heating - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Heating:Gas')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Heating - Gas (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Heating:DistrictHeating')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Heating - District Heating (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Cooling:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Cooling - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Cooling:DistrictCooling')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Cooling - District Cooling (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('InteriorLights:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Interior Lighting - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('ExteriorLights:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Exterior Lighting - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('InteriorEquipment:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - InteriorEquipment - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('InteriorEquipment:Gas')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - InteriorEquipment - Gas (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('ExteriorEquipment:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Exterior Equipment - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('ExteriorEquipment:Gas')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Exterior Equipment - Gas (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Fans:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Fans - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Pumps:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Pumps - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('HeatRejection:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Heat Rejection - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Humidifier:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Humidifier - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('Humidifier:Gas')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - Humidifier - Gas (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('WaterSystems:Electricity')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - WaterSystems - Electricity (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('WaterSystems:Gas')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Energy Usage - WaterSystems - Gas (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
        ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Output - End Use Breakdown start at 2100
    
    def Display_Order(output_metadata_list):
        x = 2100
        for metadata_dict in output_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(output_metadata_list)
    
    return outputs, output_metadata_list

def renewables():
    
    """
    Outputs that display photovoltaic electricity produced and sold by the building.
    """
    
    group_name="Results - Renewables"
    
    output_metadata_list=[]
    
    outputs=[]

# # Renewable Results include the following:

    outputs.append('Photovoltaic:ElectricityProduced')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Photovoltaic Electricity Produced (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
    outputs.append('ElectricitySurplusSold:Facility')
    
    output_metadata=({
        "Label": ("{} (ekWh/m2)".format(outputs[-1])),
        "Long Name": None,
        "Displayed Name": "Surplus Photovoltaic Electricity Sold (eKWh/m2)",
        "Description": ("**Insert Description here at future date.** [Additional Info](https://www.goodnewsnetwork.org/)"),
        "Default":None,
        "Type":"continuous",
        "Parameter Group":group_name,
        "Displayed Order": None,
        "Visibility": "Hidden",
        "Pinned": None
    })
    output_metadata_list.append(output_metadata)
    
        ## This sets the 'Display Order' automatically based on the order they appear in this set. It doesn't yet account for if some parameters are manually defined, that is forthcoming.
    ## Output - Renewables start at 2300
    
    def Display_Order(output_metadata_list):
        x = 2300
        for metadata_dict in output_metadata_list:
            if metadata_dict['Displayed Order'] == None:
                metadata_dict['Displayed Order'] = x
                x += 1
        return
    
    Display_Order(output_metadata_list)
    
    return outputs, output_metadata_list

# def unmet_hours():

#     outputs=[]

#     outputs.append

#     return outputs

    
