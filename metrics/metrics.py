import numpy as np

def duration_boosted_intensity(T, RH = None , measure = 'Temperature', indicator = 'peak'):
    '''Calculates duration boosted intensity for a given heat event
        T - array like object containing hourly zone temperature over simulation
        RH - array like object containing hourly zone humidity over simulation
        measure - string designating the intensity indicator either set to 'Temperature' or 'Apparent' (temperature)
        indicator - The temperature attribute used to define magnitude set to 'peak', 'min', 'avg'
    '''
    
    if measure == 'Temperature':
        v = T
    elif measure == 'Apparent':
        TF = (T*9/5) + 32
        HIF = -42.379 + 2.04901523*TF + 10.14333127*RH - .22475541*TF*RH - .00683783*TF*TF \
            - .05481717*RH*RH + .00122874*TF*TF*RH + .00085282*TF*RH*RH - .00000199*TF*TF*RH*RH
        HI = (HIF-32)*5/9
        v = HI
    else: 
        raise ValueError('measure ' + measure + ' not recognised')
    
    if indicator == 'peak':
        I = max(v)
    elif indicator == 'min':
        I = min(v)
    elif indicator == 'avg':
        I = sum(v)/len(v)
    else:
        raise ValueError('indicator ' + indicator + ' not recognised')
    
    d = len(v)/24
    
    dbi = d*I
    
    return(dbi)


def cumulative_metric(T, RH, ts = 'hourly', measure = 'Temperature', args = 'value'):
    '''Calculates cumulative metrics for a given heat event
        T - array like object containing hourly zone temperature over simulation
        RH - array like object containing hourly zone humidity over simulation
        ts - the times at which the metric is measured from 'hourly', 'daily', 'bidaily'
        measure - string designating the intensity indicator either set to 'Temperature' or 'Apparent' (temperature)
    '''
    
    if measure == 'Temperature':
        v = T
    elif measure == 'Apparent':
        TF = (T*9/5) + 32
        HIF = -42.379 + 2.04901523*TF + 10.14333127*RH - .22475541*TF*RH - .00683783*TF*TF \
            - .05481717*RH*RH + .00122874*TF*TF*RH + .00085282*TF*RH*RH - .00000199*TF*TF*RH*RH
        HI = (HIF-32)*5/9
        v = HI
    else: 
        raise ValueError('measure ' + measure + ' not recognised')
    
    if ts == 'hourly':
        scores = np.zeros(len(v))
        i = 0
        for value in v:
            if round(value)<28:  # When respiritory symptoms worsen
                score = -1
            elif round(value)<35:
                score = 1
            elif round(value)<40:
                score = 2
            else:
                score = 3

            scores[i] = score
            i = i+1

        HW_scores = np.zeros(len(scores))
        j = 0
        for i in range(len(scores)):
            HW_scores[i] = max([sum(scores[j:i]), HW_scores[i-1] + scores[i], 0])
            if sum(scores[i-3:i+1])<0:
                HW_scores[i] = 0
                j = i
        HW_score = max(HW_scores)
    
    elif ts == 'daily':
        scores = np.zeros(int(len(v)/24))
        for i in range(int(len(v)/24)):
            day = v[i*24:i*24+24]
            value = max(day)
            if round(value)<28:  # When respiritory symptoms worsen
                score = -1
            elif round(value)<35:
                score = 1
            elif round(value)<40:
                score = 2
            else:
                score = 3

            scores[i] = score
            
        HW_scores = np.zeros(len(scores))
        j = 0
        for i in range(len(scores)):
            HW_scores[i] = max([sum(scores[j:i]), HW_scores[i-1] + scores[i], 0])
            if sum(scores[i-1:i+1])<0:
                HW_scores[i] = 0
                j = i
        HW_score = max(HW_scores)
    
    elif ts == 'bidaily':
        scores = np.zeros(int(len(v)/24)*2)
        for i in range(int(len(v)/24)):
            day = v[i*24:i*24+24]
            value = max(day)
            min_day = min(day)
            if round(value)<28:  # When respiritory symptoms worsen
                score = -1
            elif round(value)<35:
                score = 1
            elif round(value)<40:
                score = 2
            else:
                score = 3

            # Check for tropical night - look into optimal sleeping temperatures
            if round(min_day) >= 20:
                night_score = 2
            elif round(min_day) > 18:
                night_score = 1
            else:
                night_score = -1

            scores[i*2] = score
            scores[i*2+1] = night_score
            
        HW_scores = np.zeros(len(scores))
        j = 0
        for i in range(len(scores)):
            HW_scores[i] = max([sum(scores[j:i]), HW_scores[i-1] + scores[i], 0])
            if sum(scores[i-3:i+1])<0:
                HW_scores[i] = 0
                j = i
        HW_score = max(HW_scores)
        
    if args == 'value':   
        return(HW_score)
    elif args == 'timeseries':
        return(HW_scores)

def HWMI_metric(T, RH, chunk_size, measure = 'Temperature', indicator = 'avg'):
    '''Calculates cumulative metrics for a given heat event
        T - array like object containing hourly zone temperature over simulation
        RH - array like object containing hourly zone humidity over simulation
        chunk_size - number of days included in each chunk
        measure - string designating the intensity indicator either set to 'Temperature' or 'Apparent' (temperature)
        indicator - attribute used in calculating the metric. Either set to 'avg', 'peak' or 'min'
    '''
    if measure == 'Temperature':
        v = T
    elif measure == 'Apparent':
        TF = (T*9/5) + 32
        HIF = -42.379 + 2.04901523*TF + 10.14333127*RH - .22475541*TF*RH - .00683783*TF*TF \
            - .05481717*RH*RH + .00122874*TF*TF*RH + .00085282*TF*RH*RH - .00000199*TF*TF*RH*RH
        HI = (HIF-32)*5/9
        v = HI
    else: 
        raise ValueError('measure ' + measure + ' not recognised')
        
    # Split into chunks of the size specified and calculates the mean measure value
    # Sum up over duration
    HWMI = 0
    v = np.asarray(v)
    for i in range(0, len(v), 24*chunk_size):
        chunk = v[i: i+chunk_size]
        if indicator == 'avg':
            chunk_value = np.mean(chunk)
        elif indicator == 'peak':
            chunk_value = max(chunk)
        elif indicator == 'min':
            chunk_value = min(chunk)
        HWMI = HWMI + chunk_value
    
    return(HWMI)